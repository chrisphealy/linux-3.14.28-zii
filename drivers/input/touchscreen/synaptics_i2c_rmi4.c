/**
 * Synaptics Register Mapped Interface (RMI4) I2C Physical Layer Driver.
 * Copyright (c) 2007-2010, Synaptics Incorporated
 *
 * Author: Js HA <js.ha@stericsson.com> for ST-Ericsson
 * Author: Naveen Kumar G <naveen.gaddipati@stericsson.com> for ST-Ericsson
 * Copyright 2010 (c) ST-Ericsson AB
 */
/*
 * This file is licensed under the GPL2 license.
 *
 *#############################################################################
 * GPL
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published
 * by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 *#############################################################################
 */

#undef SYNAPTICS_RMI4_USE_MULTITOUCH

#include <linux/delay.h>
#include <linux/slab.h>
#include <linux/i2c.h>
#include <linux/interrupt.h>
#include <linux/regulator/consumer.h>
#include <linux/module.h>
#include <linux/firmware.h>
#include <linux/input.h>
#include <linux/workqueue.h>
#ifdef SYNAPTICS_RMI4_USE_MULTITOUCH
#include <linux/input/mt.h>
#endif
#include <linux/input/synaptics_i2c_rmi4.h>
#include <asm/unaligned.h>

/* TODO: for multiple device support will need a per-device mutex */
#define DRIVER_NAME "synaptics_rmi4_i2c"

#define MAX_ERROR_REPORT	6
#define MAX_TOUCH_MAJOR		15
#define MAX_PRESSURE		255
#define MAX_RETRY_COUNT		5
#define STD_QUERY_LEN		21
#define PAGE_LEN		2
#define DATA_BUF_LEN		32
#define BUF_LEN			37
#define QUERY_LEN		9
#define DATA_LEN		12
#define HAS_TAP			0x01
#define HAS_PALMDETECT		0x01
#define HAS_ROTATE		0x02
#define HAS_TAPANDHOLD		0x02
#define HAS_DOUBLETAP		0x04
#define HAS_EARLYTAP		0x08
#define HAS_RELEASE		0x08
#define HAS_FLICK		0x10
#define HAS_PRESS		0x20
#define HAS_PINCH		0x40

#define HAS_QUERY12		0x20	/* in f11 query 0 register */
#define HAS_ADJUSTABLE_MAPPING	0x08	/* in f11 query 12 register */

#define MASK_16BIT		0xFFFF
#define MASK_8BIT		0xFF
#define MASK_7BIT		0x7F
#define MASK_5BIT		0x1F
#define MASK_4BIT		0x0F
#define MASK_3BIT		0x07
#define MASK_2BIT		0x03
#define TOUCHPAD_CTRL_INTR	0x8
#define PDT_START_SCAN_LOCATION (0x00E9)
#define PDT_END_SCAN_LOCATION	(0x000A)
#define PDT_ENTRY_SIZE		(0x0006)

#define RMI4_MAX_PAGE		0xff
#define RMI4_PAGE_SIZE		0x100

#define SYNAPTICS_RMI4_DEVICE_CONTROL_FUNC_NUM	0x01
#define SYNAPTICS_RMI4_TOUCHPAD_FUNC_NUM	0x11
#define SYNAPTICS_RMI4_FIRMWARE_FUNC_NUM	0x34
#define SYNAPTICS_RMI4_TEST_REPORTING_FUNC_NUM	0x54
#define SYNAPTICS_RMI4_SENSOR_TUNING_FUNC_NUM	0x55

static int max_report_size;
module_param(max_report_size, int, S_IRUGO | S_IWUSR);
MODULE_PARM_DESC(max_report_size, "Maximum report size in bytes");

/**
 * struct synaptics_rmi4_fn_desc - contains the function descriptor information
 * @query_base_addr: base address for query
 * @cmd_base_addr: base address for command
 * @ctrl_base_addr: base address for control
 * @data_base_addr: base address for data
 * @intr_src_count: count for the interrupt source
 * @fn_number: function number
 *
 * This structure is used to gives the function descriptor information
 * of the particular functionality.
 */
struct synaptics_rmi4_fn_desc {
	u16	query_base_addr;
	u16	cmd_base_addr;
	u16	ctrl_base_addr;
	u16	data_base_addr;
	u8	intr_src_count;
	u8	fn_number;
};

/* pdt entry as returned from device */

struct pdt_entry {
	unsigned char	query_base_addr;
	unsigned char	cmd_base_addr;
	unsigned char	ctrl_base_addr;
	unsigned char	data_base_addr;
	unsigned char	intr_src_count;
	unsigned char	fn_number;
};

struct synaptics_rmi4_data;

/**
 * struct synaptics_rmi4_fn - contains the function information
 * @num_of_data_points: number of fingers touched
 * @size_of_data_register_block: data register block size
 * @index_to_intr_reg: index for interrupt register
 * @intr_mask: interrupt mask value
 * @fn_desc: variable for function descriptor structure
 * @link: linked list for function descriptors
 *
 * This structure gives information about the number of data sources and
 * the number of data registers associated with the function.
 */
struct synaptics_rmi4_fn {
	struct synaptics_rmi4_fn_desc desc;
	struct list_head link;
	unsigned int intr_offset;
	int (*setup)(struct synaptics_rmi4_data *,
		     struct synaptics_rmi4_fn *);
	void (*destroy)(struct synaptics_rmi4_data *,
			struct synaptics_rmi4_fn *);
	int (*interrupt_handler)(struct synaptics_rmi4_data *,
				 struct synaptics_rmi4_fn *);
};

#define F11_QUERY_LEN	37

struct synaptics_rmi4_f11 {
	struct synaptics_rmi4_fn fn;

	u8 query_data[F11_QUERY_LEN];
	u8 control_addr;

	unsigned char num_of_data_points;
	unsigned char size_of_data_register_block;

	int sensor_max_x;
	int sensor_max_y;

#ifndef SYNAPTICS_RMI4_USE_MULTITOUCH
#define SYNAPTICS_RMI4_MAX_FINGERS	10
#define CONTACT_ID_MAX		0xffff
#define CONTACT_ID_SIGN		((CONTACT_ID_MAX + 1) >> 1)
	int			current_contact_id;
	int			contact_id[SYNAPTICS_RMI4_MAX_FINGERS];
#endif

	struct input_dev *input;
};

struct synaptics_rmi4_f01 {
	struct synaptics_rmi4_fn fn;

	u8 status;
};

struct synaptics_rmi4_f34 {
	struct synaptics_rmi4_fn fn;

	u16 block_size;
	u16 fw_blocks;
	u16 config_blocks;
	u16 ctrl_address;
	u8 status;
	struct completion cmd_done;
};

enum f54_report_types {
	F54_REPORT_NONE = 0,
	F54_8BIT_IMAGE = 1,
	F54_16BIT_IMAGE = 2,
	F54_RAW_16BIT_IMAGE = 3,
	F54_HIGH_RESISTANCE = 4,
	F54_TX_TO_TX_SHORT = 5,
	F54_RX_TO_RX1 = 7,
	F54_TRUE_BASELINE = 9,
	F54_FULL_RAW_CAP_MIN_MAX = 13,
	F54_RX_OPENS1 = 14,
	F54_TX_OPEN = 15,
	F54_TX_TO_GROUND = 16,
	F54_RX_TO_RX2 = 17,
	F54_RX_OPENS2 = 18,
	F54_FULL_RAW_CAP = 19,
	F54_FULL_RAW_CAP_RX_COUPLING_COMP = 20
};

#define F54_QUERY_LEN		27

#define F54_PERIODIC_SIZE	16
#define F54_PERIODIC_TIME	2	/* periodic timer, in seconds */
#define F54_RECAL_LIMIT		60	/* default recalibration limit */

struct synaptics_rmi4_f54 {
	struct synaptics_rmi4_fn fn;
	u8 query_data[F54_QUERY_LEN];

	u8 num_rx_electrodes;
	u8 num_tx_electrodes;
	u8 capabilities;
	u16 clock_rate;
	u8 family;

	enum f54_report_types report_type;
	u8 *report_data;
	int report_size;

	bool is_busy;
	struct mutex status_mutex;
	struct mutex data_mutex;

	struct workqueue_struct	*workqueue;
	struct delayed_work work;
	unsigned long timeout;

	struct completion cmd_done;

	/* periodic activity */
	struct mutex periodic_mutex;	/* hold while periodic report pending */

	struct workqueue_struct	*periodic;
	struct delayed_work periodic_work;

	bool is_periodic;		/* true while executing periodic report */
	u8 periodic_data[F54_PERIODIC_SIZE];
	int periodic_size;

	int worker_period;	/* rate of periodic activity, in seconds */
	int recal_limit;	/* limit to trigger recalibration */

	/* Data used for debugging */
	u8 control_addr;
	u8 data_addr;
};

#define F55_QUERY_LEN		17

struct synaptics_rmi4_f55 {
	struct synaptics_rmi4_fn fn;
	u8 query_data[F55_QUERY_LEN];

	u8 num_rx_electrodes;
	u8 num_tx_electrodes;

	/* Data used for debugging */
	u8 control_addr;
};

/**
 * struct synaptics_rmi4_device_info - contains the rmi4 device information
 * @manufacturer_id: manufacturer identification byte
 * @product_props: product properties information
 * @product_info: product info array
 * @date_code: device manufacture date
 * @tester_id: tester id array
 * @serial_number: serial number for that device
 * @product_id: product id for the device
 * @package_id: package id for the device
 * @firmware_id: firmware build id for the device
 * @support_fn_list: linked list for device information
 *
 * This structure gives information about the number of data sources and
 * the number of data registers associated with the function.
 */
struct synaptics_rmi4_device_info {
	unsigned char	manufacturer_id[4];
	unsigned char	product_props;
	unsigned char	product_info[5];
	unsigned char	date_of_manufacturing[9];
	unsigned short	tester_id;
	unsigned char	serial_number[5];
	unsigned char	product_id[11];
	unsigned char	package_id[10];
	unsigned char	firmware_id[12];
	unsigned char	bootloader_id[5];
	unsigned char	configuration_id[9];
};

/**
 * struct synaptics_rmi4_data - contains the rmi4 device data
 * @rmi4_mod_info: structure variable for rmi4 device info
 * @input_dev: pointer for input device
 * @i2c_client: pointer for i2c client
 * @board: constant pointer for touch platform data
 * @rmi4_page_mutex: mutex for rmi4 page
 * @current_page: variable for integer
 * @n_interrupt_registers: interrupt registers count
 * @regulator: pointer to the regulator structure
 * @wait: wait queue structure variable
 * @touch_stopped: flag to stop the thread function
 *
 * This structure gives the device data information.
 */
struct synaptics_rmi4_data {
	struct synaptics_rmi4_device_info info;
	struct synaptics_rmi4_f01 f01;
	struct synaptics_rmi4_f11 f11;
	struct synaptics_rmi4_f34 f34;
	struct synaptics_rmi4_f54 f54;
	struct synaptics_rmi4_f55 f55;
	struct list_head fn_list;

	int num_rx_electrodes;
	int num_tx_electrodes;

	const struct synaptics_rmi4_platform_data *board;

	struct device		*dev;
	struct i2c_client	*i2c_client;

	struct mutex		rmi4_page_mutex;
	int			current_page;

	struct mutex		flash_mutex;

	unsigned int		interrupt_count;
	unsigned int		n_interrupt_registers;

	struct regulator	*regulator;

	wait_queue_head_t	wait;
	bool			touch_stopped;

	int			update_firmware_status;
	int			update_fw_progress;
	int			update_fw_size;
	struct completion	async_firmware_done;
};

static int synaptics_rmi4_query_device(struct synaptics_rmi4_data *);
static void synaptics_rmi4_destroy_fns(struct synaptics_rmi4_data *);
static int synaptics_rmi4_parse_f11(struct synaptics_rmi4_data *,
				    struct synaptics_rmi4_fn_desc *);

/**
 * synaptics_rmi4_set_page() - sets the page
 * @rmi4: pointer to synaptics_rmi4_data structure
 * @address: set the address of the page
 *
 * This function is used to set the page and returns integer.
 */
static int synaptics_rmi4_set_page(struct synaptics_rmi4_data *rmi4,
					unsigned int address)
{
	unsigned char txbuf[PAGE_LEN];
	int retval;
	unsigned int page;
	struct i2c_client *i2c = rmi4->i2c_client;

	page = ((address >> 8) & MASK_8BIT);
	if (page != rmi4->current_page) {
		txbuf[0] = MASK_8BIT;
		txbuf[1] = page;
		retval = i2c_master_send(i2c, txbuf, PAGE_LEN);
		if (retval != PAGE_LEN) {
			if (retval >= 0)
				retval = -EIO;
			dev_err(&i2c->dev, "%s: failed:%d\n", __func__, retval);
			return retval;
		}

		rmi4->current_page = page;
	}

	return 0;
}
/**
 * synaptics_rmi4_i2c_block_read() - read the block of data
 * @rmi4: pointer to synaptics_rmi4_data structure
 * @address: read the block of data from this offset
 * @valp: pointer to a buffer containing the data to be read
 * @size: number of bytes to read
 *
 * This function is to read the block of data and returns integer.
 */
static int synaptics_rmi4_i2c_block_read(struct synaptics_rmi4_data *rmi4,
					 unsigned short address,
					 void *valp, int size)
{
	int retval;
	int retry_count = 0;
	struct i2c_client *i2c = rmi4->i2c_client;
	u8 txbuf[1] = { address & 0xff };

	mutex_lock(&rmi4->rmi4_page_mutex);
	retval = synaptics_rmi4_set_page(rmi4, address);
	if (retval)
		goto exit;

retry:
	retval = i2c_master_send(i2c, txbuf, sizeof(txbuf));
	if (retval != sizeof(txbuf))
		goto i2c_error;
	retval = i2c_master_recv(i2c, (u8 *) valp, size);
i2c_error:
	if (retval == size) {
		retval = 0;
	} else {
		if (++retry_count == MAX_RETRY_COUNT) {
			if (retval >= 0)
				retval = -EIO;
			dev_err(&i2c->dev,
				"%s: address %#04x size %d failed: %d\n",
					__func__, address, size, retval);
		} else {
			synaptics_rmi4_set_page(rmi4, address);
			goto retry;
		}
	}

exit:
	mutex_unlock(&rmi4->rmi4_page_mutex);
	return retval;
}

/**
 * synaptics_rmi4_i2c_block_write() - write the single byte data
 * @rmi4: pointer to synaptics_rmi4_data structure
 * @address: write the block of data from this offset
 * @data: data to be written
 * @len: size of the data to be written
 *
 * This function is to write the single byte data and returns integer.
 */
static int synaptics_rmi4_i2c_block_write(struct synaptics_rmi4_data *rmi4,
					  unsigned short address,
					  const void *data, size_t len)
{
	unsigned char txbuf[32];
	int retval = 0;
	struct i2c_client *i2c = rmi4->i2c_client;

	if (len >= sizeof(txbuf))
		return -EINVAL;

	/* Can't have anyone else changing the page behind our backs */
	mutex_lock(&(rmi4->rmi4_page_mutex));

	retval = synaptics_rmi4_set_page(rmi4, address);
	if (retval)
		goto exit;

	txbuf[0] = address & MASK_8BIT;
	memcpy(&txbuf[1], data, len);

	retval = i2c_master_send(rmi4->i2c_client, txbuf, len + 1);
	if (retval != len + 1) {
		if (retval >= 0)
			retval = -EIO;
		dev_err(&i2c->dev, "%s: failed: %d\n", __func__, retval);
	} else
		retval = 0;
exit:
	mutex_unlock(&rmi4->rmi4_page_mutex);
	return retval;
}

/**
 * synaptics_rmi4_i2c_byte_write() - write the single byte data
 * @rmi4: pointer to synaptics_rmi4_data structure
 * @address: write the block of data from this offset
 * @data: data to be written
 *
 * This function is to write the single byte data and returns integer.
 */
static int synaptics_rmi4_i2c_byte_write(struct synaptics_rmi4_data *rmi4,
					 unsigned short address,
					 unsigned char data)
{
	return synaptics_rmi4_i2c_block_write(rmi4, address, &data, 1);
}

/**
 * synaptics_rmi4_i2c_byte_read() - read a single byte data
 * @rmi4: pointer to synaptics_rmi4_data structure
 * @address: write the block of data from this offset
 * @data: pointer where read byte should be stored
 *
 * This function is to write the single byte data and returns integer.
 */
static int synaptics_rmi4_i2c_byte_read(struct synaptics_rmi4_data *rmi4,
					unsigned short address,
					unsigned char *data)
{
	return synaptics_rmi4_i2c_block_read(rmi4, address, data, 1);
}

/**
 * synaptics_rmi4_toggle_ints_for_fn() - control interrupt sources
 * @rmi4: pointer to synaptics_rmi4_data structure
 * @fn: function for which we want to control interrupt register(s)
 * @enable: controls whether we should enable or disable interrupt source
 *
 * This function enables or disables interrupts for given RMI4 function
 * in F01 interrupt register.
 */
static int synaptics_rmi4_toggle_ints_for_fn(struct synaptics_rmi4_data *rmi4,
					     struct synaptics_rmi4_fn *fn,
					     bool enable)
{
	int i;
	int error;
	u8 data;

	for (i = 0; i < fn->desc.intr_src_count % MASK_3BIT; i++) {
		unsigned int idx = (fn->intr_offset + i) / 8;
		unsigned int pos = (fn->intr_offset + i) % 8;
		unsigned int addr = rmi4->f01.fn.desc.ctrl_base_addr + 1 + idx;

		error = synaptics_rmi4_i2c_byte_read(rmi4, addr, &data);
		if (error) {
			dev_err(rmi4->dev,
				"%s: failed to read interrupt reg #%d: %d\n",
				__func__, idx, error);
			return error;
		}

		if (enable)
			data |= 1U << pos;
		else
			data &= ~(1U << pos);

		error = synaptics_rmi4_i2c_byte_write(rmi4, addr, data);
		if (error) {
			dev_err(rmi4->dev,
				"%s: failed to write interrupt reg #%d: %d\n",
				__func__, idx, error);
			return error;
		}
	}

	return 0;
}

/*********************************************************************
 *             Firmware Update handling                              *
 *********************************************************************/

//#define SYNAPTICS_RMI4_FIRMWARE_NAME	"synrmi4.fw"
#define SYNAPTICS_RMI4_FIRMWARE_MAX_FILE_NAME_LEN 1024
char synaptics_rmi4_firmware_name[SYNAPTICS_RMI4_FIRMWARE_MAX_FILE_NAME_LEN] = {0};

/* F34 image file offsets. */
#define F34_FW_IMAGE_OFFSET	0x100

/* F34 register offsets. */
#define F34_BLOCK_DATA_OFFSET	2

/* F34 commands */
#define F34_WRITE_FW_BLOCK	0x2
#define F34_ERASE_ALL		0x3
#define F34_READ_CONFIG_BLOCK	0x5
#define F34_WRITE_CONFIG_BLOCK	0x6
#define F34_ERASE_CONFIG	0x7
#define F34_ENABLE_FLASH_PROG	0xf

#define F34_STATUS_IN_PROGRESS	0xff
#define F34_STATUS_IDLE		0x80

#define F34_IDLE_WAIT_MS	500
#define F34_ENABLE_WAIT_MS	300
#define F34_ERASE_WAIT_MS	5000

#define F34_BOOTLOADER_ID_LEN	2

struct synaptics_rmi4_firmware {
	__le32 checksum;
	u8 pad1[3];
	u8 bootloader_version;
	__le32 image_size;
	__le32 config_size;
	u8 product_id[10];
	u8 product_info[2];
	u8 pad2[228];
	u8 data[];
};

static int synaptics_rmi4_write_bootloader_id(struct synaptics_rmi4_data *rmi4)
{
	struct synaptics_rmi4_f34 *f34 = &rmi4->f34;
	struct i2c_client *client = rmi4->i2c_client;
	u8 bootloader_id[F34_BOOTLOADER_ID_LEN];
	int error;

	error = synaptics_rmi4_i2c_block_read(rmi4,
					f34->fn.desc.query_base_addr,
					bootloader_id,
					sizeof(bootloader_id));
	if (error) {
		dev_err(&client->dev, "%s: Reading bootloader ID failed: %d\n",
			__func__, error);
		return error;
	}

	dev_dbg(rmi4->dev, "%s: writing bootloader id '%c%c'\n",
		__func__, bootloader_id[0], bootloader_id[1]);

	error = synaptics_rmi4_i2c_block_write(rmi4,
			f34->fn.desc.data_base_addr + F34_BLOCK_DATA_OFFSET,
			bootloader_id, sizeof(bootloader_id));
	if (error) {
		dev_err(&client->dev,
			"Failed to write bootloader ID: %d\n", error);
		return error;
	}

	return 0;
}

static int synaptics_rmi4_f34_command(struct synaptics_rmi4_data *rmi4,
				      u8 command, unsigned int timeout,
				      bool write_bl_id)
{
	struct synaptics_rmi4_f34 *f34 = &rmi4->f34;
	int error;

	if (write_bl_id) {
		error = synaptics_rmi4_write_bootloader_id(rmi4);
		if (error) {
			dev_err(rmi4->dev,
				"%s: failed to write bootloader id for command %#02x\n",
				__func__, command);
			return error;
		}
	}

	init_completion(&f34->cmd_done);

	error = synaptics_rmi4_i2c_byte_read(rmi4, f34->ctrl_address,
					     &f34->status);
	if (error) {
		dev_err(rmi4->dev,
			"%s: Failed to read command register: %d (command %#02x)\n",
			__func__, error, command);
		return error;
	}

	f34->status |= command & 0x0f;

	error = synaptics_rmi4_i2c_byte_write(rmi4, f34->ctrl_address,
					      f34->status);
	if (error < 0) {
		dev_err(rmi4->dev,
			"Failed to write F34 command %#02x: %d\n",
			command, error);
		return error;
	}

	if (!wait_for_completion_timeout(&f34->cmd_done,
					 msecs_to_jiffies(timeout))) {

		error = synaptics_rmi4_i2c_byte_read(rmi4, f34->ctrl_address,
						     &f34->status);
		if (error) {
			dev_err(rmi4->dev,
				"%s: failed to read status after command %#02x timed out: %d\n",
				__func__, command, error);
			return error;
		}

		if (f34->status & 0x7f) {
			dev_err(rmi4->dev,
				"%s: command %#02x timed out, fw status: %#02x\n",
				__func__, command, f34->status);
			return -ETIMEDOUT;
		}
	}

	return 0;
}

static int synaptics_rmi4_fw_intr_handler(struct synaptics_rmi4_data *rmi4,
					  struct synaptics_rmi4_fn *fn)
{
	struct synaptics_rmi4_f34 *f34 =
			container_of(fn, struct synaptics_rmi4_f34, fn);
	int error;

	dev_dbg(rmi4->dev, "%s: called for %0x\n", __func__, fn->desc.fn_number);

	error = synaptics_rmi4_i2c_byte_read(rmi4, f34->ctrl_address,
					     &f34->status);
	dev_dbg(rmi4->dev, "%s: status: %#02x, error: %d\n",
		__func__, f34->status, error);

	if (!error && !(f34->status & 0x7f))
		complete(&rmi4->f34.cmd_done);

	return 0;
}

static int synaptics_rmi4_parse_f01(struct synaptics_rmi4_data *rmi4,
				    struct synaptics_rmi4_fn_desc *fd)
{
	unsigned char std_queries[STD_QUERY_LEN];
	int error;

	/* Load up the standard queries and get the RMI4 module info */
	error = synaptics_rmi4_i2c_block_read(rmi4,
					      fd->query_base_addr,
					      std_queries,
					      sizeof(std_queries));
	if (error) {
		dev_err(rmi4->dev,
			"%s: Failed to query F01 properties: %d\n",
			__func__, error);
		return error;
	}

	/*
	 * get manufacturer id, product_props, product info,
	 * date code, tester id, serial num and product id (name)
	 */

	/* Check if this is a Synaptics device - report if not. */
	if (std_queries[0] != 1)
		dev_err(rmi4->dev, "%s: non-Synaptics mfg id:%d\n",
			__func__, std_queries[0]);

	snprintf(rmi4->info.manufacturer_id,
		 sizeof(rmi4->info.manufacturer_id),
		 "%d", std_queries[0]);

	rmi4->info.product_props = std_queries[1];

	snprintf(rmi4->info.product_info, sizeof(rmi4->info.product_info),
		 "%02x%02x", std_queries[2], std_queries[3]);

	snprintf(rmi4->info.date_of_manufacturing,
		 sizeof(rmi4->info.date_of_manufacturing),
		 "%04d%02d%02d",
		 2000 + (std_queries[4] & MASK_5BIT),
		 std_queries[5] & MASK_4BIT,
		 std_queries[6] & MASK_5BIT);

	rmi4->info.tester_id = ((std_queries[7] & MASK_7BIT) << 8) |
			        (std_queries[8] & MASK_7BIT);

	snprintf(rmi4->info.serial_number, sizeof(rmi4->info.serial_number),
		 "%04x",
		 ((std_queries[9] & MASK_7BIT) << 8) |
		  (std_queries[10] & MASK_7BIT));

	snprintf(rmi4->info.product_id, sizeof(rmi4->info.product_id),
		 "%.*s", 10, &std_queries[11]);
	dev_info(rmi4->dev, "F01 Product        ID: %s\n",
			rmi4->info.product_id);

	/*
	 * Try to read package ID and package ID revision, which
	 * we should get by reading four bytes starting at offset 17.
	 */
	error = synaptics_rmi4_i2c_block_read(rmi4,
					      fd->query_base_addr + 17,
					      std_queries, 4);
	if (error) {
		dev_err(rmi4->dev,
			"%s: Failed to query F01 properties @ offset 17: %d\n",
			__func__, error);
	} else {
		snprintf(rmi4->info.package_id, sizeof(rmi4->info.package_id),
			 "%02x%02x.%02x%02x",
			 std_queries[1], std_queries[0],
			 std_queries[3], std_queries[2]);
	}

	/* Try to read firmware build ID */
	error = synaptics_rmi4_i2c_block_read(rmi4,
					      fd->query_base_addr + 18,
					      std_queries, 3);
	if (error) {
		dev_err(rmi4->dev,
			"%s: Failed to query F01 properties @ offset 18: %d\n",
			__func__, error);
	} else {
		snprintf(rmi4->info.firmware_id, sizeof(rmi4->info.firmware_id),
			 "%d", (std_queries[2] << 16) + (std_queries[1] << 8) +
			 std_queries[0]);
		dev_info(rmi4->dev, "F01 Firmware Build ID: %s\n",
			rmi4->info.firmware_id);
	}

	error = synaptics_rmi4_i2c_byte_read(rmi4, fd->data_base_addr,
					     &rmi4->f01.status);
	if (error) {
		dev_err(rmi4->dev,
			"%s: Failed to read device status: %d\n",
			__func__, error);
		return error;
	}

	dev_dbg(rmi4->dev, "%s: device status is: %#02x\n",
		__func__, rmi4->f01.status);

	rmi4->f01.fn.desc = *fd;
	return 0;
}

static int synaptics_rmi4_parse_f34(struct synaptics_rmi4_data *rmi4,
				    struct synaptics_rmi4_fn_desc *fd)
{
	struct synaptics_rmi4_f34 *f34 = &rmi4->f34;
	unsigned char f34_queries[9];
	bool has_config_id;
	int retval;

	retval = synaptics_rmi4_i2c_block_read(rmi4,
					fd->query_base_addr,
					f34_queries,
					sizeof(f34_queries));
	if (retval) {
		dev_err(rmi4->dev, "%s: Failed to query F34 properties\n",
			__func__);
		return retval;
	}

	snprintf(rmi4->info.bootloader_id, sizeof(rmi4->info.bootloader_id),
		 "%c%c", f34_queries[0], f34_queries[1]);

	f34->fn.desc = *fd;
	f34->fn.intr_offset = rmi4->interrupt_count;
	f34->fn.interrupt_handler = synaptics_rmi4_fw_intr_handler;

	f34->block_size = get_unaligned_le16(&f34_queries[3]);
	f34->fw_blocks = get_unaligned_le16(&f34_queries[5]);
	f34->config_blocks = get_unaligned_le16(&f34_queries[7]);
	f34->ctrl_address = fd->data_base_addr + F34_BLOCK_DATA_OFFSET +
				f34->block_size;
	has_config_id = f34_queries[2] & (1 << 2);

	dev_info(rmi4->dev, "F34 Bootloader     ID: %s\n",
		rmi4->info.bootloader_id);
	dev_dbg(rmi4->dev, "F34 block size: %d\n", f34->block_size);
	dev_dbg(rmi4->dev, "F34 fw blocks: %d\n", f34->fw_blocks);
	dev_dbg(rmi4->dev, "F34 config blocks: %d\n", f34->config_blocks);

	if (has_config_id) {
		retval = synaptics_rmi4_i2c_block_read(rmi4,
						       fd->ctrl_base_addr,
						       f34_queries,
						       sizeof(f34_queries));
		if (retval) {
			dev_err(rmi4->dev,
				"Failed to read F34 configuration ID\n");
		} else {
			snprintf(rmi4->info.configuration_id,
				 sizeof(rmi4->info.configuration_id),
				 "%02x%02x%02x%02x", f34_queries[0],
				 f34_queries[1], f34_queries[2],
				 f34_queries[3]);
			dev_info(rmi4->dev,
				 "F34 Configuration  ID: %s\n",
				 rmi4->info.configuration_id);
		}
	}
	list_add_tail(&f34->fn.link, &rmi4->fn_list);
	return 0;
}

/*********************************************************************
 *                         F54 handling                              *
 *********************************************************************/

/* F54 data offsets */

#define F54_REPORT_DATA_OFFSET	3
#define F54_FIFO_OFFSET		1
#define F54_NUM_TX_OFFSET	1
#define F54_NUM_RX_OFFSET	0

/* F54 commands */

#define F54_GET_REPORT		1
#define F54_FORCE_CAL		2

/* F54 reporting capabilities */

#define F54_CAP_BASELINE	(1 << 2)
#define F54_CAP_IMAGE8		(1 << 3)
#define F54_CAP_IMAGE16		(1 << 6)

/* Fixed sizes of reports */
#define F54_FULL_RAW_CAP_MIN_MAX_SIZE	4
#define F54_HIGH_RESISTANCE_SIZE(rx, tx) \
					(2 * ((rx) * (tx) + (rx) + (tx) + 3))
#define F54_MAX_REPORT_SIZE(rx, tx)	F54_HIGH_RESISTANCE_SIZE((rx), (tx))

static void synaptics_rmi4_reset_device(struct synaptics_rmi4_data *rmi4);

static bool is_f54_report_type_valid(struct synaptics_rmi4_f54 *f54,
				     enum f54_report_types reptype)
{
	/*
	 * Basic checks on report_type to ensure we write a valid type
	 * to the sensor.
	 */
	switch (reptype) {
	case F54_8BIT_IMAGE:
		return f54->capabilities & F54_CAP_IMAGE8;
	case F54_16BIT_IMAGE:
	case F54_RAW_16BIT_IMAGE:
		return f54->capabilities & F54_CAP_IMAGE16;
	case F54_TRUE_BASELINE:
		return f54->capabilities & F54_CAP_BASELINE;
	case F54_HIGH_RESISTANCE:
	case F54_TX_TO_TX_SHORT:
	case F54_RX_TO_RX1:
	case F54_FULL_RAW_CAP_MIN_MAX:
	case F54_RX_OPENS1:
	case F54_TX_OPEN:
	case F54_TX_TO_GROUND:
	case F54_RX_TO_RX2:
	case F54_RX_OPENS2:
	case F54_FULL_RAW_CAP:
	case F54_FULL_RAW_CAP_RX_COUPLING_COMP:
		return true;
	default:
		return false;
	}
}

static int rmi4_f54_request_report(struct synaptics_rmi4_data *rmi4,
				   struct synaptics_rmi4_f54 *f54,
				   u8 report_type)
{
	int err;

	/* Write Report Type into F54_AD_Data0 */
	if (f54->report_type != report_type) {
		err = synaptics_rmi4_i2c_byte_write(rmi4,
						    f54->fn.desc.data_base_addr,
						    report_type);
		if (err)
			return err;
		f54->report_type = report_type;
	}

	/* TODO: Disable interrupts */

	/*
	 * Small delay after disabling interrupts to avoid race condition
	 * in firmare. This value is a bit higher than absolutely necessary.
	 * Should be removed once issue is resolved in firmware.
	 */
	usleep_range(2000, 3000);

	mutex_lock(&f54->data_mutex);

	err = synaptics_rmi4_i2c_byte_write(rmi4, f54->fn.desc.cmd_base_addr,
					    F54_GET_REPORT);
	if (err < 0)
		return err;

	init_completion(&f54->cmd_done);

	f54->is_busy = 1;
	f54->timeout = jiffies + msecs_to_jiffies(100);

	queue_delayed_work(f54->workqueue, &f54->work, 0);

	mutex_unlock(&f54->data_mutex);

	return 0;
}

static ssize_t rmi_f54_control_addr_show(struct device *dev,
					 struct device_attribute *attr,
					 char *buf)
{
	struct i2c_client *client = to_i2c_client(dev);
	struct synaptics_rmi4_data *rmi4 = i2c_get_clientdata(client);
	struct synaptics_rmi4_f54 *f54 = &rmi4->f54;

	return snprintf(buf, PAGE_SIZE, "0x%02x\n", f54->control_addr);
}

static u8 f54_control_reg_size[107] = {
	1, 1, 2, 1, 1, 1, 1, 1, 2, 1, 1, 2, 1, 1, 1, 0,
	0, 0, 0, 0, 1, 2, 1, 2, 2, 1, 1, 1, 2, 1, 1, 1,
	1, 1, 1, 1, 0, 0, 0, 0, 0, 1, 2, 2, 1, 1, 1, 1,
	1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 8, 2, 1, 1, 1, 1,
	1, 1, 1, 1, 1, 1, 1, 1, 2, 2, 2, 1, 1, 1, 1, 1,
	1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
	1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1
};

static ssize_t rmi_f54_control_addr_store(struct device *dev,
					  struct device_attribute *attr,
					  const char *buf, size_t count)
{
	struct i2c_client *client = to_i2c_client(dev);
	struct synaptics_rmi4_data *rmi4 = i2c_get_clientdata(client);
	struct synaptics_rmi4_f54 *f54 = &rmi4->f54;
	unsigned long val;
	int err;

	err = strict_strtoul(buf, 10, &val);
	if (err)
		return err;

	if (val >= ARRAY_SIZE(f54_control_reg_size))
		return -EINVAL;

	f54->control_addr = val;

	return count;
}

static DEVICE_ATTR(f54_control_addr, S_IRUGO | S_IWUSR,
		   rmi_f54_control_addr_show, rmi_f54_control_addr_store);

static ssize_t rmi_f54_control_data_show(struct device *dev,
					 struct device_attribute *attr,
					 char *buf)
{
	struct i2c_client *client = to_i2c_client(dev);
	struct synaptics_rmi4_data *rmi4 = i2c_get_clientdata(client);
	struct synaptics_rmi4_f54 *f54 = &rmi4->f54;
	u8 data;
	int err;

	err = synaptics_rmi4_i2c_byte_read(rmi4, f54->fn.desc.ctrl_base_addr +
					   f54->control_addr, &data);
	if (err)
		return err;

	return snprintf(buf, PAGE_SIZE, "0x%02x\n", data);
}

static ssize_t rmi_f54_control_data_store(struct device *dev,
					  struct device_attribute *attr,
					  const char *buf, size_t count)
{
	struct i2c_client *client = to_i2c_client(dev);
	struct synaptics_rmi4_data *rmi4 = i2c_get_clientdata(client);
	struct synaptics_rmi4_f54 *f54 = &rmi4->f54;
	unsigned long val;
	int err;

	err = strict_strtoul(buf, 10, &val);
	if (err)
		return err;

	if (val > 255)
		return -EINVAL;

	err = synaptics_rmi4_i2c_byte_write(rmi4, f54->fn.desc.ctrl_base_addr +
					    f54->control_addr, val);
	return err ? : count;
}

static DEVICE_ATTR(f54_control_data, S_IRUGO | S_IWUSR,
		   rmi_f54_control_data_show, rmi_f54_control_data_store);

static int f54_get_control_reg_size(struct synaptics_rmi4_f54 *f54, int reg)
{
	if (reg < 0 || reg >= ARRAY_SIZE(f54_control_reg_size))
		return 0;

	switch (reg) {
	case 15:
		return f54->num_rx_electrodes;
	case 16:
		return f54->num_tx_electrodes;
	case 17:
	case 18:
	case 19:
	case 38:
	case 39:
	case 40:
	case 75:
		return f54->query_data[12 + 1] & 0x0f;
	case 36:
		switch (f54->query_data[8] & 0x03) {
		case 0x01:
			return max(f54->num_tx_electrodes,
				   f54->num_rx_electrodes);
		case 0x02:
			return f54->num_rx_electrodes;
		}
		return 0;
	case 37:
		if ((f54->query_data[8] & 0x03) == 0x02)
			return f54->num_rx_electrodes;
		return 0;
	case 64:
		return 7;	/* or 1 */
	case 87:
		return 13;
	case 89:
		return 11;
	case 91:
		return 5;
	case 93:
		return 2;
	case 94:
		return 4;
	case 95:
		return 11 * (f54->query_data[12 + 1] & 0x0f);
	case 96:
		return f54->num_rx_electrodes;
	case 97:
		return f54->num_rx_electrodes + f54->num_tx_electrodes;
	case 98:
		return 10;
	case 99:
		if (f54->family == 2)
			return 3;
		return 0;
	case 101:
		return 5;
	case 102:
		return 28;
	case 103:
		return 0;
	case 104:
		return 8;
	default:
		break;
	}
	return f54_control_reg_size[reg];
}

static ssize_t rmi_f54_control_data_read(struct file *data_file,
					 struct kobject *kobj,
					 struct bin_attribute *attr,
					 char *buf, loff_t pos, size_t count)
{
	struct device *dev = container_of(kobj, struct device, kobj);
	struct i2c_client *client = to_i2c_client(dev);
	struct synaptics_rmi4_data *rmi4 = i2c_get_clientdata(client);
	struct synaptics_rmi4_f54 *f54 = &rmi4->f54;
	size_t fsize = f54_get_control_reg_size(f54, f54->control_addr);
	int error;

	if (count == 0)
		return 0;

	if (pos > fsize)
		return -EFBIG;

	if (pos + count > fsize)
		count = fsize - pos;

	if (count == 0)
		return count;

	error = synaptics_rmi4_i2c_block_read(rmi4,
					      rmi4->f54.fn.desc.ctrl_base_addr
							+ f54->control_addr,
					      buf, count);
	if (error < 0)
		return error;
	return count;
}

static struct bin_attribute f54_control_data = {
	.attr = {
		 .name = "f54_control_data_bin",
		 .mode = S_IRUGO
	},
	.size = 0,
	.read = rmi_f54_control_data_read,
};

static ssize_t rmi_f54_data_addr_show(struct device *dev,
				      struct device_attribute *attr,
				      char *buf)
{
	struct i2c_client *client = to_i2c_client(dev);
	struct synaptics_rmi4_data *rmi4 = i2c_get_clientdata(client);
	struct synaptics_rmi4_f54 *f54 = &rmi4->f54;

	return snprintf(buf, PAGE_SIZE, "0x%02x\n", f54->data_addr);
}

static ssize_t rmi_f54_data_addr_store(struct device *dev,
				       struct device_attribute *attr,
				       const char *buf, size_t count)
{
	struct i2c_client *client = to_i2c_client(dev);
	struct synaptics_rmi4_data *rmi4 = i2c_get_clientdata(client);
	struct synaptics_rmi4_f54 *f54 = &rmi4->f54;
	unsigned long val;
	int err;

	err = strict_strtoul(buf, 10, &val);
	if (err)
		return err;

	if (val > 19 + 6)
		return -EINVAL;

	f54->data_addr = val;

	return count;
}

static DEVICE_ATTR(f54_data_addr, S_IRUGO | S_IWUSR,
		   rmi_f54_data_addr_show, rmi_f54_data_addr_store);

static ssize_t rmi_f54_data_data_show(struct device *dev,
				      struct device_attribute *attr, char *buf)
{
	struct i2c_client *client = to_i2c_client(dev);
	struct synaptics_rmi4_data *rmi4 = i2c_get_clientdata(client);
	struct synaptics_rmi4_f54 *f54 = &rmi4->f54;
	u8 data;
	int err;

	err = synaptics_rmi4_i2c_byte_read(rmi4, f54->fn.desc.data_base_addr +
					   f54->data_addr, &data);
	if (err)
		return err;

	return snprintf(buf, PAGE_SIZE, "0x%02x\n", data);
}

static ssize_t rmi_f54_data_data_store(struct device *dev,
				       struct device_attribute *attr,
				       const char *buf, size_t count)
{
	struct i2c_client *client = to_i2c_client(dev);
	struct synaptics_rmi4_data *rmi4 = i2c_get_clientdata(client);
	struct synaptics_rmi4_f54 *f54 = &rmi4->f54;
	unsigned long val;
	int err;

	err = strict_strtoul(buf, 10, &val);
	if (err)
		return err;

	if (val > 255)
		return -EINVAL;

	err = synaptics_rmi4_i2c_byte_write(rmi4, f54->fn.desc.data_base_addr +
					    f54->data_addr, val);
	return err ? : count;
}

static DEVICE_ATTR(f54_data_data, S_IRUGO | S_IWUSR,
		   rmi_f54_data_data_show, rmi_f54_data_data_store);

static ssize_t rmi_f54_command_show(struct device *dev,
				      struct device_attribute *attr,
				      char *buf)
{
	struct i2c_client *client = to_i2c_client(dev);
	struct synaptics_rmi4_data *rmi4 = i2c_get_clientdata(client);
	struct synaptics_rmi4_f54 *f54 = &rmi4->f54;
	u8 data;
	int err;

	err = synaptics_rmi4_i2c_byte_read(rmi4, f54->fn.desc.cmd_base_addr,
					   &data);
	if (err)
		return err;

	return snprintf(buf, PAGE_SIZE, "0x%02x\n", data);
}

static ssize_t rmi_f54_command_store(struct device *dev,
				       struct device_attribute *attr,
				       const char *buf, size_t count)
{
	struct i2c_client *client = to_i2c_client(dev);
	struct synaptics_rmi4_data *rmi4 = i2c_get_clientdata(client);
	struct synaptics_rmi4_f54 *f54 = &rmi4->f54;
	unsigned long val;
	int err;

	err = strict_strtoul(buf, 10, &val);
	if (err)
		return err;

	if (val > 255)
		return -EINVAL;

	err = synaptics_rmi4_i2c_byte_write(rmi4, f54->fn.desc.cmd_base_addr,
					    val);
	return err ? : count;
}

static DEVICE_ATTR(f54_command, S_IRUGO | S_IWUSR,
		   rmi_f54_command_show, rmi_f54_command_store);

static ssize_t rmi_f54_report_type_show(struct device *dev,
					struct device_attribute *attr,
					char *buf)
{
	struct i2c_client *client = to_i2c_client(dev);
	struct synaptics_rmi4_data *rmi4 = i2c_get_clientdata(client);
	struct synaptics_rmi4_f54 *f54 = &rmi4->f54;

	return snprintf(buf, PAGE_SIZE, "%u\n", f54->report_type);
}

static ssize_t rmi_f54_report_type_store(struct device *dev,
					 struct device_attribute *attr,
					 const char *buf, size_t count)
{
	struct i2c_client *client = to_i2c_client(dev);
	struct synaptics_rmi4_data *rmi4 = i2c_get_clientdata(client);
	struct synaptics_rmi4_f54 *f54 = &rmi4->f54;
	unsigned long val;
	int err;

	err = strict_strtoul(buf, 10, &val);
	if (err)
		return err;

	if (val && !is_f54_report_type_valid(f54, val))
		return -EINVAL;

	mutex_lock(&f54->periodic_mutex);

	mutex_lock(&f54->status_mutex);
	if (f54->is_busy) {
		err = -EBUSY;
		goto error;
	}

	if (val == 0) {
		synaptics_rmi4_reset_device(rmi4);
		f54->report_type = 0;
	} else {
		err = rmi4_f54_request_report(rmi4, f54, val);
	}
error:
	mutex_unlock(&f54->status_mutex);
	mutex_unlock(&f54->periodic_mutex);

	return err < 0 ? err : count;
}

static DEVICE_ATTR(f54_report_type, S_IRUGO | S_IWUSR,
		   rmi_f54_report_type_show, rmi_f54_report_type_store);

static ssize_t rmi_f54_recal_period_show(struct device *dev,
					 struct device_attribute *attr,
					 char *buf)
{
	struct i2c_client *client = to_i2c_client(dev);
	struct synaptics_rmi4_data *rmi4 = i2c_get_clientdata(client);
	struct synaptics_rmi4_f54 *f54 = &rmi4->f54;

	return snprintf(buf, PAGE_SIZE, "%d\n", f54->worker_period);
}

static ssize_t rmi_f54_recal_period_store(struct device *dev,
					  struct device_attribute *attr,
					  const char *buf, size_t count)
{
	struct i2c_client *client = to_i2c_client(dev);
	struct synaptics_rmi4_data *rmi4 = i2c_get_clientdata(client);
	struct synaptics_rmi4_f54 *f54 = &rmi4->f54;
	unsigned long val;
	int err;

	err = strict_strtoul(buf, 10, &val);
	if (err)
		return err;

	if (val > 255)
		return -EINVAL;

	mutex_lock(&f54->periodic_mutex);
	f54->worker_period = val;

	cancel_delayed_work_sync(&f54->periodic_work);
	if (f54->worker_period)
		queue_delayed_work(f54->periodic, &f54->periodic_work,
				   msecs_to_jiffies(f54->worker_period * 1000));

	mutex_unlock(&f54->periodic_mutex);

	return count;
}

static DEVICE_ATTR(f54_recal_period, S_IRUGO | S_IWUSR,
		   rmi_f54_recal_period_show, rmi_f54_recal_period_store);

static ssize_t rmi_f54_recal_limit_show(struct device *dev,
					struct device_attribute *attr,
					char *buf)
{
	struct i2c_client *client = to_i2c_client(dev);
	struct synaptics_rmi4_data *rmi4 = i2c_get_clientdata(client);
	struct synaptics_rmi4_f54 *f54 = &rmi4->f54;

	return snprintf(buf, PAGE_SIZE, "%d\n", f54->recal_limit);
}

static ssize_t rmi_f54_recal_limit_store(struct device *dev,
					 struct device_attribute *attr,
					 const char *buf, size_t count)
{
	struct i2c_client *client = to_i2c_client(dev);
	struct synaptics_rmi4_data *rmi4 = i2c_get_clientdata(client);
	struct synaptics_rmi4_f54 *f54 = &rmi4->f54;
	unsigned long val;
	int err;

	err = strict_strtoul(buf, 10, &val);
	if (err)
		return err;

	if (val > 65535)
		return -EINVAL;

	f54->recal_limit = val;

	return count;
}

static DEVICE_ATTR(f54_recal_limit, S_IRUGO | S_IWUSR,
		   rmi_f54_recal_limit_show, rmi_f54_recal_limit_store);

/* Provide access to last non-periodic report */
static ssize_t rmi_f54_report_data_read(struct file *data_file,
					struct kobject *kobj,
					struct bin_attribute *attr,
					char *buf, loff_t pos, size_t count)
{
	struct device *dev = container_of(kobj, struct device, kobj);
	struct i2c_client *client = to_i2c_client(dev);
	struct synaptics_rmi4_data *rmi4 = i2c_get_clientdata(client);
	struct synaptics_rmi4_f54 *f54 = &rmi4->f54;
	ssize_t ret;

	if (count == 0)
		return 0;

	mutex_lock(&f54->periodic_mutex);
	mutex_lock(&f54->data_mutex);

	while (f54->is_busy) {
		mutex_unlock(&f54->data_mutex);
		/*
		 * transfer can take a long time, so wait for up to one second
		 */
		if (!wait_for_completion_timeout(&f54->cmd_done,
						 msecs_to_jiffies(1000)))
			return -ETIMEDOUT;
		mutex_lock(&f54->data_mutex);
	}
	if (f54->report_size == 0) {
		ret = -ENODATA;
		goto done;
	}

	if (pos + count > f54->report_size)
		count = f54->report_size - pos;

	memcpy(buf, f54->report_data + pos, count);
	ret = count;
done:
	mutex_unlock(&f54->data_mutex);
	mutex_unlock(&f54->periodic_mutex);

	return ret;
}

static struct bin_attribute f54_report_data = {
	.attr = {
		 .name = "f54_report_data",
		 .mode = S_IRUGO
	},
	.size = 0,
	.read = rmi_f54_report_data_read,
};

static ssize_t rmi_f54_query_data_read(struct file *data_file,
				       struct kobject *kobj,
				       struct bin_attribute *attr,
				       char *buf, loff_t pos, size_t count)
{
	struct device *dev = container_of(kobj, struct device, kobj);
	struct i2c_client *client = to_i2c_client(dev);
	struct synaptics_rmi4_data *rmi4 = i2c_get_clientdata(client);
	struct synaptics_rmi4_f54 *f54 = &rmi4->f54;

	if (count == 0)
		return 0;

	if (pos > F54_QUERY_LEN)
		return -EFBIG;

	if (pos + count > F54_QUERY_LEN)
		count = F54_QUERY_LEN - pos;

	memcpy(buf, f54->query_data + pos, count);
	return count;
}

static struct bin_attribute f54_query_data = {
	.attr = {
		 .name = "f54_query_data",
		 .mode = S_IRUGO
	},
	.size = 0,
	.read = rmi_f54_query_data_read,
};

static int f54_get_report_size(struct synaptics_rmi4_data *rmi4,
			       struct synaptics_rmi4_f54 *f54)
{
	u8 rx = rmi4->num_rx_electrodes ? : f54->num_rx_electrodes;
	u8 tx = rmi4->num_tx_electrodes ? : f54->num_tx_electrodes;
	int size = 0;

	switch (f54->report_type) {
	case F54_8BIT_IMAGE:
		size = rx * tx;
		break;
	case F54_16BIT_IMAGE:
	case F54_RAW_16BIT_IMAGE:
	case F54_TRUE_BASELINE:
	case F54_FULL_RAW_CAP:
	case F54_FULL_RAW_CAP_RX_COUPLING_COMP:
		size = 2 * rx * tx;
		break;
	case F54_HIGH_RESISTANCE:
		size = F54_HIGH_RESISTANCE_SIZE(rx, tx);
		break;
	case F54_FULL_RAW_CAP_MIN_MAX:
		size = F54_FULL_RAW_CAP_MIN_MAX_SIZE;
		break;
	case F54_TX_TO_TX_SHORT:
	case F54_TX_OPEN:
	case F54_TX_TO_GROUND:
		size =  (tx + 7) / 8;
		break;
	case F54_RX_TO_RX1:
	case F54_RX_OPENS1:
		if (rx < tx)
			size = 2 * rx * rx;
		else
			size = 2 * rx * tx;
		break;
	case F54_RX_TO_RX2:
	case F54_RX_OPENS2:
		if (rx <= tx)
			size = 0;
		else
			size = 2 * rx * (rx - tx);
		break;
	default:
		size = 0;
	}

	if (max_report_size > 0 && size > max_report_size)
		size = max_report_size;

	return size;
}

/*
 *  rmi4_f54_periodic_work()
 */
static void rmi4_f54_periodic_work(struct work_struct *work)
{
	struct synaptics_rmi4_f54 *f54 =
		container_of(work, struct synaptics_rmi4_f54,
			     periodic_work.work);
	struct synaptics_rmi4_data *rmi4 =
		container_of(f54, struct synaptics_rmi4_data, f54);
	int err, i, sum;
	s16 *data;

	mutex_lock(&f54->periodic_mutex);
	f54->is_periodic = true;

	mutex_lock(&f54->status_mutex);
	if (f54->is_busy) {
		mutex_unlock(&f54->status_mutex);
		goto reschedule;
	}

	err = rmi4_f54_request_report(rmi4, f54, F54_16BIT_IMAGE);
	if (err) {
		mutex_unlock(&f54->status_mutex);
		goto reschedule;
	}
	mutex_unlock(&f54->status_mutex);

	/* Now wait for results */

	mutex_lock(&f54->data_mutex);

	while (f54->is_busy) {
		mutex_unlock(&f54->data_mutex);
		/*
		 * If we did not receive a response within 200 ms,
		 * just try again later.
		 */
		if (!wait_for_completion_timeout(&f54->cmd_done,
						 msecs_to_jiffies(200))) {
			goto reschedule;
		}
		mutex_lock(&f54->data_mutex);
	}
	if (f54->periodic_size == 0)
		goto data_done;

	data = (s16 *)f54->periodic_data;
	sum = 0;
	for (i = 0; i < F54_PERIODIC_SIZE / 2; i++)
		sum += data[i];


//	print_hex_dump(KERN_INFO, "calibration data: ", DUMP_PREFIX_NONE, 16, 1,
//		       f54->periodic_data, f54->periodic_size, false);

	dev_dbg(rmi4->dev, "calibration data sum: %d\n", sum);
	if (sum > f54->recal_limit) {
		dev_info(rmi4->dev, "Touchscreen watchdog sum = %d Forcing recalibration.\n", sum);
		err = synaptics_rmi4_i2c_byte_write(rmi4,
						    f54->fn.desc.cmd_base_addr,
						    F54_FORCE_CAL);
	}

data_done:
	mutex_unlock(&f54->data_mutex);
reschedule:
	queue_delayed_work(f54->periodic, &f54->periodic_work,
			   msecs_to_jiffies(f54->worker_period * 1000));
	f54->is_periodic = false;
	mutex_unlock(&f54->periodic_mutex);
}

struct f54_reports {
	int start;
	int size;
};

static struct f54_reports f54_periodic_report_24[] = {
	{ 0   * 2, 4 },		/* four bytes each from four corners */
	{ 37  * 2, 4 },
	{ 896 * 2, 4 },
	{ 934 * 2, 4 },		/* This must be the last two blocks in the 936 block buffer */
	{ 0, 0 },
};

static struct f54_reports f54_periodic_report_30[] = {
	{ 0    * 2, 4 },	/* four bytes each from four corners */
	{ 44   * 2, 4 },
	{ 1421 * 2, 4 },
	{ 1378 * 2, 4 },	/* This must be the last two blocks in the 1380 block buffer */
	{ 0, 0 },
};

static struct f54_reports f54_standard_report[] = {
    { 0, 0 },		/* report size to be added in code */
    { 0, 0 },
};

/*
 *  rmi4_f54_work()
 */
static void rmi4_f54_work(struct work_struct *work)
{
	struct synaptics_rmi4_f54 *f54 =
		container_of(work, struct synaptics_rmi4_f54, work.work);
	struct synaptics_rmi4_data *rmi4 =
		container_of(f54, struct synaptics_rmi4_data, f54);
	u8 fifo[2];
	struct f54_reports *report;
	int report_size;
	u8 command;
	u8 *data;
	int err;

	if (f54->is_periodic) {
		data = f54->periodic_data;
		if (rmi4->num_tx_electrodes <= 24)
			report = f54_periodic_report_24;
		else
			report = f54_periodic_report_30;
	} else {

		data = f54->report_data;
		report_size = f54_get_report_size(rmi4, f54);
		if (report_size == 0) {
			dev_err(rmi4->dev, "Bad report size, report type=%d\n",
				f54->report_type);
			err = -EINVAL;
			goto error;	/* retry won't help */
		}
		f54_standard_report[0].size = report_size;
		report = f54_standard_report;
	}

	mutex_lock(&f54->data_mutex);

	/*
	 * Need to check if command has completed.
	 * If not try again later.
	 */
	err = synaptics_rmi4_i2c_byte_read(rmi4, f54->fn.desc.cmd_base_addr,
					   &command);
	if (err) {
		dev_err(rmi4->dev, "Failed to read back command\n");
		goto error;
	}
	if (command & F54_GET_REPORT) {
		if (time_after(jiffies, f54->timeout)) {
			dev_err(rmi4->dev, "Get report command timed out\n");
			err = -ETIMEDOUT;
		}
		report_size = 0;
		goto error;
	}

	dev_dbg(rmi4->dev, "Get report command completed, reading data\n");

	report_size = 0;
	for (; report->size; report++) {
	    fifo[0] = report->start & 0xff;
	    fifo[1] = (report->start >> 8) & 0xff;
	    err = synaptics_rmi4_i2c_block_write(rmi4,
					     f54->fn.desc.data_base_addr
						+ F54_FIFO_OFFSET,
					     fifo, sizeof(fifo));
	    if (err) {
		dev_err(rmi4->dev, "Failed to set fifo start offset\n");
		goto abort;
	    }

	    err = synaptics_rmi4_i2c_block_read(rmi4,
					    f54->fn.desc.data_base_addr
						+ F54_REPORT_DATA_OFFSET,
					    data, report->size);
	    if (err) {
	        dev_err(rmi4->dev, "Report data read [%d bytes] returned %d\n",
			report->size, err);
		goto abort;
	    }
	    data += report->size;
	    report_size += report->size;
	}

abort:
	if (!f54->is_periodic)
		f54->report_size = err ? 0 : report_size;
	else
		f54->periodic_size = err ? 0 : report_size;
error:
	if (err)
		report_size = 0;

	if (report_size == 0 && !err) {
		queue_delayed_work(f54->workqueue, &f54->work,
				   msecs_to_jiffies(1));
	} else {
		f54->is_busy = false;
		complete(&f54->cmd_done);
	}

	mutex_unlock(&f54->data_mutex);
}

static struct attribute *synaptics_f54_attrs[] = {
	&dev_attr_f54_report_type.attr,
	&dev_attr_f54_control_addr.attr,
	&dev_attr_f54_control_data.attr,
	&dev_attr_f54_data_addr.attr,
	&dev_attr_f54_data_data.attr,
	&dev_attr_f54_command.attr,
	&dev_attr_f54_recal_period.attr,
	&dev_attr_f54_recal_limit.attr,
	NULL
};

static struct attribute_group synaptics_f54_attr_group = {
	.attrs		= synaptics_f54_attrs,
};

static int synaptics_rmi4_setup_f54(struct synaptics_rmi4_data *rmi4,
				    struct synaptics_rmi4_fn *fn)
{
	struct synaptics_rmi4_f54 *f54 =
			container_of(fn, struct synaptics_rmi4_f54, fn);
	int ret;
	u8 rx, tx;

	mutex_init(&f54->data_mutex);
	mutex_init(&f54->status_mutex);
	mutex_init(&f54->periodic_mutex);

	rx = f54->num_rx_electrodes;
	tx = f54->num_tx_electrodes;
	f54->report_data = devm_kzalloc(rmi4->dev,
					F54_MAX_REPORT_SIZE(rx, tx),
					GFP_KERNEL);
	if (f54->report_data == NULL)
		return -ENOMEM;

	INIT_DELAYED_WORK(&f54->work, rmi4_f54_work);
	INIT_DELAYED_WORK(&f54->periodic_work, rmi4_f54_periodic_work);

	f54->worker_period = F54_PERIODIC_TIME;
	f54->recal_limit = F54_RECAL_LIMIT;

	f54->workqueue = create_singlethread_workqueue("rmi4-poller");
	if (!f54->workqueue)
		return -ENOMEM;

	f54->periodic = create_singlethread_workqueue("rmi4-periodic");
	if (!f54->periodic) {
		ret = -ENOMEM;
		goto remove_wq;
	}

	ret = sysfs_create_bin_file(&rmi4->dev->kobj, &f54_report_data);
	if (ret < 0)
		goto remove_periodic;

	ret = sysfs_create_bin_file(&rmi4->dev->kobj, &f54_query_data);
	if (ret < 0)
		goto remove_report;

	ret = sysfs_create_bin_file(&rmi4->dev->kobj, &f54_control_data);
	if (ret < 0)
		goto remove_query;

	ret = sysfs_create_group(&rmi4->dev->kobj, &synaptics_f54_attr_group);
	if (ret)
		goto remove_control;

	if (f54->worker_period)
		queue_delayed_work(f54->periodic, &f54->periodic_work,
				   msecs_to_jiffies(f54->worker_period * 1000));

	return 0;

remove_control:
	sysfs_remove_bin_file(&rmi4->dev->kobj, &f54_control_data);
remove_query:
	sysfs_remove_bin_file(&rmi4->dev->kobj, &f54_query_data);
remove_report:
	sysfs_remove_bin_file(&rmi4->dev->kobj, &f54_report_data);
remove_periodic:
	cancel_delayed_work_sync(&f54->periodic_work);
	flush_workqueue(f54->periodic);
	destroy_workqueue(f54->periodic);
remove_wq:
	cancel_delayed_work_sync(&f54->work);
	flush_workqueue(f54->workqueue);
	destroy_workqueue(f54->workqueue);

	return ret;
}

static void synaptics_rmi4_destroy_f54(struct synaptics_rmi4_data *rmi4,
				       struct synaptics_rmi4_fn *fn)
{
	struct synaptics_rmi4_f54 *f54 =
			container_of(fn, struct synaptics_rmi4_f54, fn);

	sysfs_remove_group(&rmi4->dev->kobj, &synaptics_f54_attr_group);
	sysfs_remove_bin_file(&rmi4->dev->kobj, &f54_control_data);
	sysfs_remove_bin_file(&rmi4->dev->kobj, &f54_report_data);
	sysfs_remove_bin_file(&rmi4->dev->kobj, &f54_query_data);

	cancel_delayed_work_sync(&f54->periodic_work);
	flush_workqueue(f54->periodic);
	destroy_workqueue(f54->periodic);

	cancel_delayed_work_sync(&f54->work);
	flush_workqueue(f54->workqueue);
	destroy_workqueue(f54->workqueue);
}

static int synaptics_rmi4_parse_f54(struct synaptics_rmi4_data *rmi4,
				    struct synaptics_rmi4_fn_desc *fd)
{
	struct synaptics_rmi4_f54 *f54 = &rmi4->f54;
	int retval;

	retval = synaptics_rmi4_i2c_block_read(rmi4, fd->query_base_addr,
					       f54->query_data,
					       sizeof(f54->query_data));
	if (retval) {
		dev_err(rmi4->dev, "%s: Failed to query F54 properties\n",
			__func__);
		return retval;
	}

	f54->fn.desc = *fd;
	f54->fn.intr_offset = rmi4->interrupt_count;
#if 0 /* don't use interrupts */
	f54->fn.interrupt_handler = synaptics_rmi4_f54_intr_handler;
#endif
	f54->fn.setup = synaptics_rmi4_setup_f54;
	f54->fn.destroy = synaptics_rmi4_destroy_f54;

	f54->num_rx_electrodes = f54->query_data[0];
	f54->num_tx_electrodes = f54->query_data[1];
	f54->capabilities = f54->query_data[2];
	f54->clock_rate = f54->query_data[3] | (f54->query_data[4] << 8);
	f54->family = f54->query_data[5];

	dev_dbg(rmi4->dev, "F54 num_rx_electrodes: %d\n",
		f54->num_rx_electrodes);
	dev_dbg(rmi4->dev, "F54 num_tx_electrodes: %d\n",
		f54->num_tx_electrodes);

	dev_dbg(rmi4->dev, "F54 capabilities: 0x%x\n", f54->capabilities);
	dev_dbg(rmi4->dev, "F54 clock rate: 0x%x\n", f54->clock_rate);
	dev_dbg(rmi4->dev, "F54 family: 0x%x\n", f54->family);

	f54->is_busy = false;

	list_add_tail(&f54->fn.link, &rmi4->fn_list);

	return 0;
}

static ssize_t rmi_f55_query_data_read(struct file *data_file,
				       struct kobject *kobj,
				       struct bin_attribute *attr,
				       char *buf, loff_t pos, size_t count)
{
	struct device *dev = container_of(kobj, struct device, kobj);
	struct i2c_client *client = to_i2c_client(dev);
	struct synaptics_rmi4_data *rmi4 = i2c_get_clientdata(client);
	struct synaptics_rmi4_f55 *f55 = &rmi4->f55;

	if (count == 0)
		return 0;

	if (pos > F55_QUERY_LEN)
		return -EFBIG;

	if (pos + count > F55_QUERY_LEN)
		count = F55_QUERY_LEN - pos;

	memcpy(buf, f55->query_data + pos, count);
	return count;
}

static struct bin_attribute f55_query_data = {
	.attr = {
		 .name = "f55_query_data",
		 .mode = S_IRUGO
	},
	.size = 0,
	.read = rmi_f55_query_data_read,
};

static ssize_t rmi_f55_control_addr_show(struct device *dev,
					 struct device_attribute *attr,
					 char *buf)
{
	struct i2c_client *client = to_i2c_client(dev);
	struct synaptics_rmi4_data *rmi4 = i2c_get_clientdata(client);
	struct synaptics_rmi4_f55 *f55 = &rmi4->f55;

	return snprintf(buf, PAGE_SIZE, "0x%02x\n", f55->control_addr);
}

#define F55_CONTROL_REGS	16

static ssize_t rmi_f55_control_addr_store(struct device *dev,
					  struct device_attribute *attr,
					  const char *buf, size_t count)
{
	struct i2c_client *client = to_i2c_client(dev);
	struct synaptics_rmi4_data *rmi4 = i2c_get_clientdata(client);
	struct synaptics_rmi4_f55 *f55 = &rmi4->f55;
	unsigned long val;
	int err;

	err = strict_strtoul(buf, 10, &val);
	if (err)
		return err;

	if (val >= F55_CONTROL_REGS)
		return -EINVAL;

	f55->control_addr = val;

	return count;
}

static DEVICE_ATTR(f55_control_addr, S_IRUGO | S_IWUSR,
		   rmi_f55_control_addr_show, rmi_f55_control_addr_store);

static ssize_t rmi_f55_control_data_show(struct device *dev,
					 struct device_attribute *attr,
					 char *buf)
{
	struct i2c_client *client = to_i2c_client(dev);
	struct synaptics_rmi4_data *rmi4 = i2c_get_clientdata(client);
	struct synaptics_rmi4_f55 *f55 = &rmi4->f55;
	u8 data;
	int err;

	err = synaptics_rmi4_i2c_byte_read(rmi4, f55->fn.desc.ctrl_base_addr +
					   f55->control_addr, &data);
	if (err)
		return err;

	return snprintf(buf, PAGE_SIZE, "0x%02x\n", data);
}

static ssize_t rmi_f55_control_data_store(struct device *dev,
					  struct device_attribute *attr,
					  const char *buf, size_t count)
{
	struct i2c_client *client = to_i2c_client(dev);
	struct synaptics_rmi4_data *rmi4 = i2c_get_clientdata(client);
	struct synaptics_rmi4_f55 *f55 = &rmi4->f55;
	unsigned long val;
	int err;

	err = strict_strtoul(buf, 10, &val);
	if (err)
		return err;

	if (val > 255)
		return -EINVAL;

	err = synaptics_rmi4_i2c_byte_write(rmi4, f55->fn.desc.ctrl_base_addr +
					    f55->control_addr, val);
	return err ? : count;
}

static DEVICE_ATTR(f55_control_data, S_IRUGO | S_IWUSR,
		   rmi_f55_control_data_show, rmi_f55_control_data_store);

static int f55_get_control_reg_size(struct synaptics_rmi4_f55 *f55, int reg)
{
	if (reg < 0 || reg >= F55_CONTROL_REGS)
		return 0;

	switch (reg) {
	case 0:
		return 1;
	case 1:
		return f55->num_rx_electrodes;
	case 2:
		return f55->num_tx_electrodes;
	case 3:
		return 8;
	case 4:
		switch ((f55->query_data[2] >> 2) & 0x03) {
		case 0x01:
			return max(f55->num_rx_electrodes,
				   f55->num_tx_electrodes);
		case 0x02:
			return f55->num_rx_electrodes;
		}
		break;
	case 5:
		if (((f55->query_data[2] >> 2) & 0x03) == 0x02)
			return f55->num_tx_electrodes;
		break;
	case 6:
		return 0;
	case 7:
		return f55->num_tx_electrodes;
	case 8:
		return f55->query_data[8];
	case 9:
		return f55->query_data[4];
	case 10:
		return 8;
	case 11:
		return f55->query_data[6] + f55->query_data[7];
	case 12:
		return f55->num_rx_electrodes;
	case 13:
		return 1;
	case 14:
		return 10;
	case 15:
		return 6 + f55->query_data[11] + f55->query_data[12] +
			f55->query_data[13] + f55->query_data[14] +
			f55->query_data[15] + f55->query_data[16];
	default:
		break;
	}
	return 0;
}

static ssize_t rmi_f55_control_data_read(struct file *data_file,
					 struct kobject *kobj,
					 struct bin_attribute *attr,
					 char *buf, loff_t pos, size_t count)
{
	struct device *dev = container_of(kobj, struct device, kobj);
	struct i2c_client *client = to_i2c_client(dev);
	struct synaptics_rmi4_data *rmi4 = i2c_get_clientdata(client);
	struct synaptics_rmi4_f55 *f55 = &rmi4->f55;
	size_t fsize = f55_get_control_reg_size(f55, f55->control_addr);
	int error;

	if (count == 0)
		return 0;

	if (pos > fsize)
		return -EFBIG;

	if (pos + count > fsize)
		count = fsize - pos;

	if (count == 0)
		return count;

	error = synaptics_rmi4_i2c_block_read(rmi4,
					      rmi4->f55.fn.desc.ctrl_base_addr
							+ f55->control_addr,
					      buf, count);
	if (error < 0)
		return error;
	return count;
}

static struct bin_attribute f55_control_data = {
	.attr = {
		 .name = "f55_control_data_bin",
		 .mode = S_IRUGO
	},
	.size = 0,
	.read = rmi_f55_control_data_read,
};

static int synaptics_rmi4_setup_f55(struct synaptics_rmi4_data *rmi4,
				    struct synaptics_rmi4_fn *fn)
{
	int ret;

	ret = sysfs_create_bin_file(&rmi4->dev->kobj, &f55_query_data);
	if (ret < 0)
		return ret;

	ret = sysfs_create_bin_file(&rmi4->dev->kobj, &f55_control_data);
	if (ret < 0)
		goto remove_query;

	return 0;

remove_query:
	sysfs_remove_bin_file(&rmi4->dev->kobj, &f55_query_data);
	return ret;
}

static void synaptics_rmi4_destroy_f55(struct synaptics_rmi4_data *rmi4,
				       struct synaptics_rmi4_fn *fn)
{
	sysfs_remove_bin_file(&rmi4->dev->kobj, &f55_query_data);
	sysfs_remove_bin_file(&rmi4->dev->kobj, &f55_control_data);
}

static int synaptics_rmi4_parse_f55(struct synaptics_rmi4_data *rmi4,
				    struct synaptics_rmi4_fn_desc *fd)
{
	struct synaptics_rmi4_f55 *f55 = &rmi4->f55;
	int retval;

	retval = synaptics_rmi4_i2c_block_read(rmi4, fd->query_base_addr,
					       f55->query_data,
					       sizeof(f55->query_data));
	if (retval) {
		dev_err(rmi4->dev, "%s: Failed to query F55 properties\n",
			__func__);
		return retval;
	}

	f55->fn.desc = *fd;
	f55->fn.intr_offset = rmi4->interrupt_count;

	f55->fn.setup = synaptics_rmi4_setup_f55;
	f55->fn.destroy = synaptics_rmi4_destroy_f55;

	f55->num_rx_electrodes = f55->query_data[0];
	f55->num_tx_electrodes = f55->query_data[1];

	rmi4->num_rx_electrodes = f55->num_rx_electrodes;
	rmi4->num_tx_electrodes = f55->num_tx_electrodes;


	if (f55->query_data[2] & 0x01) {
		/* HasSensorAssignment is true */
		int i, total;
		u8 buf[256];

		retval = synaptics_rmi4_i2c_block_read(rmi4,
					rmi4->f55.fn.desc.ctrl_base_addr + 1,
					buf, f55->num_rx_electrodes);
		if (!retval) {
		    total = 0;
		    for (i = 0; i < f55->num_rx_electrodes; i++) {
			if (buf[i] != 0xff)
				total++;
		    }
		    rmi4->num_rx_electrodes = total;
		}
		retval = synaptics_rmi4_i2c_block_read(rmi4,
					rmi4->f55.fn.desc.ctrl_base_addr + 2,
					buf, f55->num_tx_electrodes);
		if (!retval) {
		    total = 0;
		    for (i = 0; i < f55->num_tx_electrodes; i++) {
			if (buf[i] != 0xff)
				total++;
		    }
		    rmi4->num_tx_electrodes = total;
		}
	}

	dev_info(rmi4->dev, "F55 num_rx_electrodes: %d [used %d]\n",
		f55->num_rx_electrodes, rmi4->num_rx_electrodes);
	dev_info(rmi4->dev, "F55 num_tx_electrodes: %d [used %d]\n",
		f55->num_tx_electrodes, rmi4->num_tx_electrodes);

	list_add_tail(&f55->fn.link, &rmi4->fn_list);

	return 0;
}

static int synaptics_rmi4_scan_pdt(struct synaptics_rmi4_data *rmi4,
				   bool allow_all)
{
	struct synaptics_rmi4_fn_desc fd;
	struct pdt_entry pdt;
	u16 addr;
	int error;
	int page;
	bool done = false;
	int maxpage = allow_all ? RMI4_MAX_PAGE : 0;

	rmi4->interrupt_count = 0;

	for (page = 0; page <= maxpage && !done; page++) {
	    u16 page_start = RMI4_PAGE_SIZE * page;
	    u16 pdt_start = page_start + PDT_START_SCAN_LOCATION;
	    u16 pdt_end = page_start + PDT_END_SCAN_LOCATION;

	    dev_dbg(rmi4->dev, "Scan page %d start 0x%x end 0x%x\n",
		     page, pdt_start, pdt_end);

	    done = true;
	    for (addr = pdt_start; addr >= pdt_end; addr -= sizeof(pdt)) {

		error = synaptics_rmi4_i2c_block_read(rmi4, addr,
						&pdt, sizeof(pdt));
		if (error) {
			dev_err(rmi4->dev, "%s: read error at %#04x\n",
				__func__, addr);
			return error;
		}

		dev_dbg(rmi4->dev, "%s: page: %d pdt: %x %x %x %x %x %x\n",
			__func__,
			page,
			pdt.query_base_addr, pdt.cmd_base_addr,
			pdt.ctrl_base_addr, pdt.data_base_addr,
			pdt.intr_src_count, pdt.fn_number);

		if (pdt.fn_number == 0 || pdt.fn_number == 0xff)
			break;

		fd.query_base_addr = pdt.query_base_addr + page_start;
		fd.cmd_base_addr = pdt.cmd_base_addr + page_start;
		fd.ctrl_base_addr = pdt.ctrl_base_addr + page_start;
		fd.data_base_addr = pdt.data_base_addr + page_start;
		fd.intr_src_count = pdt.intr_src_count & MASK_3BIT;
		fd.fn_number = pdt.fn_number;

		done = false;

		switch (fd.fn_number) {
		case SYNAPTICS_RMI4_DEVICE_CONTROL_FUNC_NUM:
			error = synaptics_rmi4_parse_f01(rmi4, &fd);
			if (error)
				return error;
			/*
			 * Restrict mode if touchscreen is in firmware download
			 * state.
			 */
			if (allow_all && (rmi4->f01.status & 0x40)) {
				dev_alert(rmi4->dev,
					  "Touchscreen is in firmware download state.\n");
				allow_all = false;
				maxpage = 0;
			}
			break;

		case SYNAPTICS_RMI4_FIRMWARE_FUNC_NUM:
			error = synaptics_rmi4_parse_f34(rmi4, &fd);
			if (error)
				return error;
			break;

		case SYNAPTICS_RMI4_TEST_REPORTING_FUNC_NUM:
			if (allow_all) {
				error = synaptics_rmi4_parse_f54(rmi4, &fd);
				if (error)
					return error;
			}
			break;

		case SYNAPTICS_RMI4_SENSOR_TUNING_FUNC_NUM:
			if (allow_all) {
				error = synaptics_rmi4_parse_f55(rmi4, &fd);
				if (error)
					return error;
			}
			break;

		case SYNAPTICS_RMI4_TOUCHPAD_FUNC_NUM:
			if (allow_all && fd.intr_src_count) {
				error = synaptics_rmi4_parse_f11(rmi4, &fd);
				if (error)
					return error;
			}
			break;
		}

		rmi4->interrupt_count += fd.intr_src_count & MASK_3BIT;
	    }
	}

	rmi4->n_interrupt_registers = DIV_ROUND_UP(rmi4->interrupt_count, 8);
	return 0;
}

static int synaptics_rmi4_write_blocks(struct synaptics_rmi4_data *rmi4,
					const void *data, int block_count,
					u8 command)
{
	struct synaptics_rmi4_f34 *f34 = &rmi4->f34;
	u16 address = f34->fn.desc.data_base_addr + F34_BLOCK_DATA_OFFSET;
	u8 start_address[] = { 0, 0 };
	int i;
	int error;

	error = synaptics_rmi4_i2c_block_write(rmi4, f34->fn.desc.data_base_addr,
						start_address,
						sizeof(start_address));
	if (error) {
		dev_err(rmi4->dev,
			"%s: Failed to write initial zeros: %d\n",
			__func__, error);
		return error;
	}

	for (i = 0; i < block_count; i++) {
		error = synaptics_rmi4_i2c_block_write(rmi4, address,
						data, f34->block_size);
		if (error) {
			dev_err(rmi4->dev,
				"%s: failed to write block #%d: %d\n",
				__func__, i, error);
			return error;
		}

		error = synaptics_rmi4_f34_command(rmi4, command,
						   F34_IDLE_WAIT_MS, false);
		if (error) {
			dev_err(rmi4->dev,
				"%s: failed to write command for block #%d: %d\n",
				__func__, i, error);
			return error;
		}

		dev_dbg(rmi4->dev, "%s: wrote block %d of %d\n",
			__func__, i + 1, block_count);

		data += f34->block_size;
		rmi4->update_fw_progress += f34->block_size;
		rmi4->update_firmware_status = (rmi4->update_fw_progress * 100) /
						rmi4->update_fw_size;
	}

	return 0;
}

static int synaptics_rmi4_write_firmware(struct synaptics_rmi4_data *rmi4,
					  const void *data)
{
	return synaptics_rmi4_write_blocks(rmi4, data, rmi4->f34.fw_blocks,
					   F34_WRITE_FW_BLOCK);
}

static int synaptics_rmi4_write_config(struct synaptics_rmi4_data *rmi4,
					  const void *data)
{
	return synaptics_rmi4_write_blocks(rmi4, data, rmi4->f34.config_blocks,
					   F34_WRITE_CONFIG_BLOCK);
}

static int synaptics_rmi4_flash_firmware(struct synaptics_rmi4_data *rmi4,
				const struct synaptics_rmi4_firmware *syn_fw)
{
	int error;

	error = synaptics_rmi4_f34_command(rmi4, F34_ENABLE_FLASH_PROG,
					   F34_ENABLE_WAIT_MS, true);
	if (error)
		return error;

	synaptics_rmi4_destroy_fns(rmi4);

	memset(&rmi4->f01, 0, sizeof(rmi4->f01));
	memset(&rmi4->f11, 0, sizeof(rmi4->f11));
	memset(&rmi4->f34, 0, sizeof(rmi4->f34));
	memset(&rmi4->f54, 0, sizeof(rmi4->f54));
	memset(&rmi4->f55, 0, sizeof(rmi4->f55));

	/* Rescan PDT only looking for F01 and F34 (should be nothing else). */
	error = synaptics_rmi4_scan_pdt(rmi4, false);
	if (error)
		return error;

	if (!rmi4->f01.fn.desc.fn_number) {
		dev_err(rmi4->dev, "%s: failed to find F01 PDT entry\n",
			__func__);
		return -ENODEV;
	}

	if (!(rmi4->f01.status & 0x40)) {
		dev_err(rmi4->dev,
			"%s: Flash programming bit is not set in device status: %d\n",
			__func__, rmi4->f01.status);
		return -EIO;
	}

	if (!rmi4->f34.fn.desc.fn_number) {
		dev_err(rmi4->dev, "%s: failed to find F34 PDT entry\n",
			__func__);
		return -ENODEV;
	}

	error = synaptics_rmi4_toggle_ints_for_fn(rmi4, &rmi4->f34.fn, true);
	if (error) {
		dev_err(rmi4->dev,
			"%s: Failed to enable interrupts for F34: %d\n",
			__func__, error);
		return error;
	}

	rmi4->update_fw_progress = 0;
	rmi4->update_fw_size = syn_fw->image_size + syn_fw->config_size;

	if (syn_fw->image_size) {
		dev_info(rmi4->dev, "%s: Erasing FW...\n", __func__);
		error = synaptics_rmi4_f34_command(rmi4, F34_ERASE_ALL,
						   F34_ERASE_WAIT_MS, true);
		if (error)
			return error;

		dev_info(rmi4->dev, "%s: Writing firmware data (%d bytes)...\n",
			__func__, syn_fw->image_size);
		error = synaptics_rmi4_write_firmware(rmi4, syn_fw->data);
		if (error)
			return error;
	}

	if (syn_fw->config_size) {
		/*
		 * We only need to erase config if we haven't updated
		 * firmware.
		 */
		if (!syn_fw->image_size) {
			dev_info(rmi4->dev, "%s: Erasing config data...\n",
				__func__);
			error = synaptics_rmi4_f34_command(rmi4, F34_ERASE_CONFIG,
							   F34_ERASE_WAIT_MS,
							   true);
			if (error)
				return error;
		}

		dev_info(rmi4->dev, "%s: Writing config data (%d bytes)...\n",
			__func__, syn_fw->config_size);
		error = synaptics_rmi4_write_config(rmi4,
					&syn_fw->data[syn_fw->image_size]);
		if (error)
			return error;
	}

	dev_info(rmi4->dev, "%s: Firmware update complete\n", __func__);
	return 0;
}

static void synaptics_rmi4_reset_device(struct synaptics_rmi4_data *rmi4)
{
	int error;

	dev_info(rmi4->dev, "%s: Resetting device...\n", __func__);

	error = synaptics_rmi4_i2c_byte_write(rmi4,
					      rmi4->f01.fn.desc.cmd_base_addr,
					      0x1);
	if (error)
		dev_err(rmi4->dev,
			"%s: post-flash reset failed: %d\n",
			__func__, error);
	msleep(100);

	dev_info(rmi4->dev, "%s: Complete\n", __func__);
}

static int synaptics_rmi4_update_firmware(const struct firmware *fw,
					  struct synaptics_rmi4_data *rmi4)
{
	struct synaptics_rmi4_f34 *f34 = &rmi4->f34;
	const struct synaptics_rmi4_firmware *syn_fw;
	int retval;
	int error;

	rmi4->update_firmware_status = 0;

	dev_info(rmi4->dev, "%s: Firmware update started\n",
		 __func__);

	syn_fw = (const struct synaptics_rmi4_firmware *)fw->data;
	BUILD_BUG_ON(offsetof(struct synaptics_rmi4_firmware, data) !=
		     F34_FW_IMAGE_OFFSET);

	dev_info(rmi4->dev,
		 "%s: FW size %d, checksum: %08x, image size: %d, config size: %d\n",
		 __func__, (int)fw->size,
		 le32_to_cpu(syn_fw->checksum),
		 le32_to_cpu(syn_fw->image_size),
		 le32_to_cpu(syn_fw->config_size));
	dev_info(rmi4->dev,
		 "%s: FW bootloader id: %02x, product id: %.*s, info: %02x%02x\n",
		 __func__, syn_fw->bootloader_version,
		 (int)sizeof(syn_fw->product_id), syn_fw->product_id,
		 syn_fw->product_info[0], syn_fw->product_info[1]);

	if (syn_fw->image_size &&
	    syn_fw->image_size != f34->fw_blocks * f34->block_size) {
		dev_err(rmi4->dev,
			"%s: Bad firmware image: fw size %d, expected %d\n",
			__func__, syn_fw->image_size,
			f34->fw_blocks * f34->block_size);
		retval = -EILSEQ;
		goto out;
	}

	if (syn_fw->config_size &&
	    syn_fw->config_size != f34->config_blocks * f34->block_size) {
		dev_err(rmi4->dev,
			"%s: Bad firmware image: config size %d, expected %d\n",
			__func__, syn_fw->config_size,
			f34->config_blocks * f34->block_size);
		retval = -EILSEQ;
		goto out;
	}

	if (syn_fw->image_size && !syn_fw->config_size) {
		dev_err(rmi4->dev,
			"%s: Bad firmware image: new firmware without config data\n",
			__func__);
		retval = -EILSEQ;
		goto out;
	}

	mutex_lock(&rmi4->flash_mutex);

	retval = synaptics_rmi4_flash_firmware(rmi4, syn_fw);
	dev_info(rmi4->dev, "%s: Firmware update completed, status: %d\n",
		 __func__, retval);

	/*
	 * Regardless of the status of firmware update we need to try to
	 * restore device to operating state.
	 */
	synaptics_rmi4_reset_device(rmi4);
	synaptics_rmi4_destroy_fns(rmi4);

	error = synaptics_rmi4_query_device(rmi4);
	if (error)
		dev_err(rmi4->dev,
			"%s: Failed re-initialize device after flashing: %d\n",
			__func__, error);

	mutex_unlock(&rmi4->flash_mutex);

out:
	rmi4->update_firmware_status = retval;
	return retval;
}


/*********************************************************************
 *             Sysfs attributes handling                             *
 *********************************************************************/

struct synaptics_rmi4_attr {
	struct device_attribute dattr;
	size_t field_offset;
	int field_length;
};

static ssize_t synaptics_rmi4_attr_show(struct device *dev,
					struct device_attribute *dattr,
					char *buf)
{
	struct i2c_client *client = to_i2c_client(dev);
	struct synaptics_rmi4_data *rmi4 = i2c_get_clientdata(client);
	struct synaptics_rmi4_attr *attr =
			container_of(dattr, struct synaptics_rmi4_attr, dattr);
	char *field = (char *)(&rmi4->info) + attr->field_offset;

	return scnprintf(buf, PAGE_SIZE, "%.*s\n", attr->field_length, field);
}

#define SYNAPTICS_RMI4_ATTR(_field)					\
struct synaptics_rmi4_attr synaptics_rmi4_attr_##_field = {		\
	.dattr = __ATTR(_field, 0644,					\
			synaptics_rmi4_attr_show, NULL),		\
	.field_offset = offsetof(struct synaptics_rmi4_device_info,	\
				 _field),				\
	.field_length = sizeof(((struct synaptics_rmi4_device_info *)	\
				 NULL)->_field),			\
}

static SYNAPTICS_RMI4_ATTR(bootloader_id);
static SYNAPTICS_RMI4_ATTR(configuration_id);
static SYNAPTICS_RMI4_ATTR(product_id);
static SYNAPTICS_RMI4_ATTR(package_id);
static SYNAPTICS_RMI4_ATTR(firmware_id);
static SYNAPTICS_RMI4_ATTR(manufacturer_id);
static SYNAPTICS_RMI4_ATTR(product_info);
static SYNAPTICS_RMI4_ATTR(serial_number);
static SYNAPTICS_RMI4_ATTR(date_of_manufacturing);


static ssize_t
synaptics_rmi4_set_firmware_file_name(struct device *dev,
				     struct device_attribute *dattr,
				     const char *buf, size_t count)
{
	/* Copy the name of the firmware file to the global */
	strncpy(synaptics_rmi4_firmware_name, buf, (SYNAPTICS_RMI4_FIRMWARE_MAX_FILE_NAME_LEN - 1) );

	/* Using echo from the command line adds a new line to the end of the string and it
	 * appears request_firmware can't handle the new line character and returns
	 * an error even if the file does exist
	 */
	if(synaptics_rmi4_firmware_name[strlen(synaptics_rmi4_firmware_name) - 1] == 0xA) {
		synaptics_rmi4_firmware_name[strlen(synaptics_rmi4_firmware_name) - 1] = 0x0;
	}
	return strlen(buf);
}

static ssize_t
synaptics_rmi4_get_firmware_file_name(struct device *dev,
				     struct device_attribute *dattr,
				     char *buf)
{
	/* Return the name of the firmware file */
	return scnprintf(buf, strlen(synaptics_rmi4_firmware_name), "%s", synaptics_rmi4_firmware_name);
}

static DEVICE_ATTR(firmware_file_name, S_IRWXU,
		synaptics_rmi4_get_firmware_file_name,
		synaptics_rmi4_set_firmware_file_name);


static ssize_t
synaptics_rmi4_update_firmware_store(struct device *dev,
				     struct device_attribute *dattr,
				     const char *buf, size_t count)
{
	struct i2c_client *client = to_i2c_client(dev);
	struct synaptics_rmi4_data *rmi4 = i2c_get_clientdata(client);
	const struct firmware *fw;
	unsigned long value;
	int error;

	error = strict_strtoul(buf, 0, &value);
	if (error)
		return error;

	if (value != 1)
		return -EINVAL;

	rmi4->update_firmware_status = 0;

	if(strlen(synaptics_rmi4_firmware_name) == 0) {
		/* Firmware file name has not been set */
		dev_err(rmi4->dev, "Firmware file name has not been set.");
		rmi4->update_firmware_status = -EPERM;  /* Operation not permitted */
		return -EPERM;  /* Operation not permitted */
	}

	error = request_firmware(&fw, synaptics_rmi4_firmware_name, rmi4->dev);
	if (error) {
		dev_err(rmi4->dev, "Failed to request firmware %s, error: %d\n",
				synaptics_rmi4_firmware_name, error);
		rmi4->update_firmware_status = error;
		return error;
	}

	error = synaptics_rmi4_update_firmware(fw, rmi4);
	release_firmware(fw);

	return error ?: count;
}

static DEVICE_ATTR(update_firmware, S_IWUSR,
		   NULL, synaptics_rmi4_update_firmware_store);

static ssize_t
synaptics_rmi4_update_firmware_status_show(struct device *dev,
					   struct device_attribute *dattr,
					   char *buf)
{
	struct i2c_client *client = to_i2c_client(dev);
	struct synaptics_rmi4_data *rmi4 = i2c_get_clientdata(client);

	return scnprintf(buf, PAGE_SIZE, "%d\n", rmi4->update_firmware_status);
}

static DEVICE_ATTR(update_firmware_status, S_IRUGO,
		   synaptics_rmi4_update_firmware_status_show, NULL);

/* f01 attribute handling */

#define F01_RESET	0x01	/* Reset command */

static ssize_t rmi_f01_command_store(struct device *dev,
				     struct device_attribute *attr,
				     const char *buf, size_t count)
{
	struct i2c_client *client = to_i2c_client(dev);
	struct synaptics_rmi4_data *rmi4 = i2c_get_clientdata(client);
	struct synaptics_rmi4_f01 *f01 = &rmi4->f01;
	unsigned long val;
	int err;

	err = strict_strtoul(buf, 10, &val);
	if (err)
		return err;

	if (val != 1)
		return -EINVAL;

	err = synaptics_rmi4_i2c_byte_write(rmi4, f01->fn.desc.cmd_base_addr,
					    F01_RESET);

	return err < 0 ? err : count;
}

static DEVICE_ATTR(f01_command, S_IWUSR, NULL, rmi_f01_command_store);

/* f11 attribute handling */

#define F11_REZERO	0x01	/* Rezero command */

static ssize_t rmi_f11_command_store(struct device *dev,
				     struct device_attribute *attr,
				     const char *buf, size_t count)
{
	struct i2c_client *client = to_i2c_client(dev);
	struct synaptics_rmi4_data *rmi4 = i2c_get_clientdata(client);
	struct synaptics_rmi4_f11 *f11 = &rmi4->f11;
	unsigned long val;
	int err;

	err = strict_strtoul(buf, 10, &val);
	if (err)
		return err;

	if (val != 1)		/* 1: Rezero */
		return -EINVAL;

	err = synaptics_rmi4_i2c_byte_write(rmi4, f11->fn.desc.cmd_base_addr,
					    F11_REZERO);

	return err < 0 ? err : count;
}

static DEVICE_ATTR(f11_command, S_IWUSR, NULL, rmi_f11_command_store);

static u8 f11_control_reg_size[100] = {
	1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
	1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
	2, 2, 1, 1, 1, 1, 1, 1, 2, 2, 2, 2, 1, 1, 1, 1,
	1, 1, 1, 1, 1, 1, 2, 1, 1, 1, 1, 1, 1, 1, 1, 1,
	1, 1,18, 1, 6, 1, 1, 1, 1, 1, 1, 3, 3, 1, 1, 1,
	1, 1, 0, 0, 0, 0, 0, 0, 0, 1, 5, 3, 6, 1, 1, 1,
	1, 3, 4, 1
};

static int f11_get_control_reg_size(struct synaptics_rmi4_f11 *f11, int reg)
{
	if (reg < 0 || reg >= ARRAY_SIZE(f11_control_reg_size))
		return 0;
	if (reg == 88)
		return f11->query_data[31];

	return f11_control_reg_size[reg];
}

static ssize_t rmi_f11_control_addr_show(struct device *dev,
					 struct device_attribute *attr,
					 char *buf)
{
	struct i2c_client *client = to_i2c_client(dev);
	struct synaptics_rmi4_data *rmi4 = i2c_get_clientdata(client);
	struct synaptics_rmi4_f11 *f11 = &rmi4->f11;

	return snprintf(buf, PAGE_SIZE, "0x%02x\n", f11->control_addr);
}

static ssize_t rmi_f11_control_addr_store(struct device *dev,
					  struct device_attribute *attr,
					  const char *buf, size_t count)
{
	struct i2c_client *client = to_i2c_client(dev);
	struct synaptics_rmi4_data *rmi4 = i2c_get_clientdata(client);
	struct synaptics_rmi4_f11 *f11 = &rmi4->f11;
	unsigned long val;
	int err;

	err = strict_strtoul(buf, 10, &val);
	if (err)
		return err;

	if (f11_get_control_reg_size(f11, val) <= 0)
		return -EINVAL;

	f11->control_addr = val;

	return count;
}

static DEVICE_ATTR(f11_control_addr, S_IRUGO | S_IWUSR,
		   rmi_f11_control_addr_show, rmi_f11_control_addr_store);

static ssize_t rmi_f11_control_data_show(struct device *dev,
					 struct device_attribute *attr,
					 char *buf)
{
	struct i2c_client *client = to_i2c_client(dev);
	struct synaptics_rmi4_data *rmi4 = i2c_get_clientdata(client);
	struct synaptics_rmi4_f11 *f11 = &rmi4->f11;
	u8 data;
	int err;

	err = synaptics_rmi4_i2c_byte_read(rmi4, f11->fn.desc.ctrl_base_addr +
					   f11->control_addr, &data);
	if (err)
		return err;

	return snprintf(buf, PAGE_SIZE, "0x%02x\n", data);
}

static ssize_t rmi_f11_control_data_store(struct device *dev,
					  struct device_attribute *attr,
					  const char *buf, size_t count)
{
	struct i2c_client *client = to_i2c_client(dev);
	struct synaptics_rmi4_data *rmi4 = i2c_get_clientdata(client);
	struct synaptics_rmi4_f11 *f11 = &rmi4->f11;
	unsigned long val;
	int err;

	err = strict_strtoul(buf, 10, &val);
	if (err)
		return err;

	if (val > 255)
		return -EINVAL;

	err = synaptics_rmi4_i2c_byte_write(rmi4, f11->fn.desc.ctrl_base_addr +
					    f11->control_addr, val);
	return err ? : count;
}

static DEVICE_ATTR(f11_control_data, S_IRUGO | S_IWUSR,
		   rmi_f11_control_data_show, rmi_f11_control_data_store);

static ssize_t rmi_f11_control_data_read(struct file *data_file,
					 struct kobject *kobj,
					 struct bin_attribute *attr,
					 char *buf, loff_t pos, size_t count)
{
	struct device *dev = container_of(kobj, struct device, kobj);
	struct i2c_client *client = to_i2c_client(dev);
	struct synaptics_rmi4_data *rmi4 = i2c_get_clientdata(client);
	struct synaptics_rmi4_f11 *f11 = &rmi4->f11;
	size_t fsize = f11_get_control_reg_size(f11, f11->control_addr);
	int error;

	if (count == 0)
		return 0;

	if (pos > fsize)
		return -EFBIG;

	if (pos + count > fsize)
		count = fsize - pos;

	if (count == 0)
		return count;

	error = synaptics_rmi4_i2c_block_read(rmi4,
					      rmi4->f11.fn.desc.ctrl_base_addr
							+ f11->control_addr,
					      buf, count);
	if (error < 0)
		return error;
	return count;
}

static struct bin_attribute f11_control_data = {
	.attr = {
		 .name = "f11_control_data_bin",
		 .mode = S_IRUGO
	},
	.size = 0,
	.read = rmi_f11_control_data_read,
};

static struct attribute *synaptics_rmi4_attrs[] = {
	&synaptics_rmi4_attr_bootloader_id.dattr.attr,
	&synaptics_rmi4_attr_configuration_id.dattr.attr,
	&synaptics_rmi4_attr_product_id.dattr.attr,
	&synaptics_rmi4_attr_package_id.dattr.attr,
	&synaptics_rmi4_attr_firmware_id.dattr.attr,
	&synaptics_rmi4_attr_manufacturer_id.dattr.attr,
	&synaptics_rmi4_attr_product_info.dattr.attr,
	&synaptics_rmi4_attr_serial_number.dattr.attr,
	&synaptics_rmi4_attr_date_of_manufacturing.dattr.attr,
	&dev_attr_firmware_file_name.attr,
	&dev_attr_update_firmware.attr,
	&dev_attr_update_firmware_status.attr,
	&dev_attr_f01_command.attr,
	&dev_attr_f11_command.attr,
	&dev_attr_f11_control_addr.attr,
	&dev_attr_f11_control_data.attr,
	&dev_attr_f55_control_addr.attr,
	&dev_attr_f55_control_data.attr,
	NULL
};

static umode_t synaptics_rmi4_is_attr_visible(struct kobject *kobj,
					      struct attribute *attr, int n)
{
	struct device *dev = container_of(kobj, struct device, kobj);
	struct i2c_client *client = to_i2c_client(dev);
	struct synaptics_rmi4_data *rmi4 = i2c_get_clientdata(client);
	umode_t mode = attr->mode;

	if ((attr == &dev_attr_update_firmware_status.attr ||
	     attr == &dev_attr_update_firmware.attr ||
	     attr == &synaptics_rmi4_attr_bootloader_id.dattr.attr ||
	     attr == &synaptics_rmi4_attr_configuration_id.dattr.attr) &&
	    !rmi4->f34.fn.desc.fn_number)
		mode = 0;

	if (attr == &dev_attr_f01_command.attr
	    && !rmi4->f01.fn.desc.fn_number)
		mode = 0;

	if ((attr == &dev_attr_f11_command.attr ||
	     attr == &dev_attr_f11_control_addr.attr ||
	     attr == &dev_attr_f11_control_data.attr)
	    && !rmi4->f11.fn.desc.fn_number)
		mode = 0;

	if ((attr == &dev_attr_f55_control_addr.attr ||
	     attr == &dev_attr_f55_control_data.attr)
	    && !rmi4->f55.fn.desc.fn_number)
		mode = 0;

	return mode;
}

static struct attribute_group synaptics_rmi4_attr_group = {
	.is_visible	= synaptics_rmi4_is_attr_visible,
	.attrs		= synaptics_rmi4_attrs,
};

/**
 * synaptics_rmi4_sensor_report() - reports to input subsystem
 * @pdata: pointer to synaptics_rmi4_data structure
 *
 * This function is used to reads in all data sources and reports
 * them to the input subsystem.
 */
static int synaptics_rmi4_sensor_report(struct synaptics_rmi4_data *rmi4)
{
	struct synaptics_rmi4_fn *fn;
	unsigned char intr_status[4];
	int touch = 0; /* number of touch points - fingers or buttons */
	int i;
	int error;

	/*
	 * Get the interrupt status from the function $01
	 * control register+1 to find which source(s) were interrupting
	 * so we can read the data from the source(s) (2D sensor, buttons..)
	 */
	error = synaptics_rmi4_i2c_block_read(rmi4,
					rmi4->f01.fn.desc.data_base_addr + 1,
					intr_status,
					rmi4->n_interrupt_registers);
	if (error) {
		dev_err(rmi4->dev,
			"%s: could not read interrupt status registers: %d\n",
			__func__, error);
		return 0;
	}

	/*
	 * check each function that has data sources and if the interrupt for
	 * that triggered then call that RMI4 functions report() function to
	 * gather data and report it to the input subsystem
	 */
	list_for_each_entry(fn, &rmi4->fn_list, link) {
		for (i = 0; i < (fn->desc.intr_src_count & MASK_3BIT); i++) {
			unsigned int idx = (fn->intr_offset + i) / 8;
			unsigned int pos = (fn->intr_offset + i) % 8;

			if ((intr_status[idx] & (1U << pos))) {
				dev_dbg(rmi4->dev,
					"%s: intr_status[%d]=%#02x, pos: %d, fn: %#02x\n",
					__func__,
					idx, intr_status[idx], pos,
					fn->desc.fn_number);
				if (fn->interrupt_handler)
					touch = fn->interrupt_handler(rmi4, fn);
				else
					WARN_ONCE(1, "fn %#02x interrupt but no handler\n",
						  fn->desc.fn_number);
			}
		}
	}

	/* return the number of touch points */
	return touch;
}

/**
 * synaptics_rmi4_irq() - thread function for rmi4 attention line
 * @irq: irq value
 * @data: void pointer
 *
 * This function is interrupt thread function. It just notifies the
 * application layer that attention is required.
 */
static irqreturn_t synaptics_rmi4_irq(int irq, void *data)
{
	struct synaptics_rmi4_data *rmi4 = data;
	int touch_count;

	do {
		touch_count = synaptics_rmi4_sensor_report(rmi4);
		if (touch_count)
			wait_event_timeout(rmi4->wait, rmi4->touch_stopped,
					   msecs_to_jiffies(1));
		else
			break;
	} while (!rmi4->touch_stopped);

	return IRQ_HANDLED;
}

/**
 * synaptics_rmi4_touchpad_report() - reports for the rmi4 touchpad device
 * @pdata: pointer to synaptics_rmi4_data structure
 * @rfi: pointer to synaptics_rmi4_fn structure
 *
 * This function calls to reports for the rmi4 touchpad device
 */
static int synaptics_rmi4_touchpad_report(struct synaptics_rmi4_data *rmi4,
					  struct synaptics_rmi4_fn *fn)
{
	struct synaptics_rmi4_f11 *f11 =
			container_of(fn, struct synaptics_rmi4_f11, fn);
	struct input_dev *input = f11->input;
	/* number of touch points - fingers down in this case */
	int	touch_count = 0;
	int	finger;
	int	finger_registers;
	int	reg;
	int	finger_shift;
	int	finger_status;
	int	error;
	unsigned int	x, y;
	unsigned int	wx, wy;
	unsigned int	z;
#ifndef SYNAPTICS_RMI4_USE_MULTITOUCH
	unsigned int	st_x = 0;
	unsigned int	st_y = 0;
	unsigned int	st_z = 0;
	int st_id = -1;
#endif
	unsigned short	data_offset;
	unsigned char	data_reg_blk_size;
	unsigned char	values[2];
	unsigned char	data[DATA_LEN];
	unsigned char	fingers_supported = f11->num_of_data_points;

	/* get 2D sensor finger data */
	/*
	 * First get the finger status field - the size of the finger status
	 * field is determined by the number of finger supporte - 2 bits per
	 * finger, so the number of registers to read is:
	 * registerCount = ceil(numberOfFingers/4).
	 * Read the required number of registers and check each 2 bit field to
	 * determine if a finger is down:
	 *	00 = finger not present,
	 *	01 = finger present and data accurate,
	 *	10 = finger present but data may not be accurate,
	 *	11 = reserved for product use.
	 */
	finger_registers = DIV_ROUND_UP(fingers_supported, 4);

	error = synaptics_rmi4_i2c_block_read(rmi4,
				f11->fn.desc.data_base_addr,
				values, finger_registers);
	if (error) {
		dev_err(rmi4->dev, "%s: read of status registers failed: %d\n",
			__func__, error);
		return 0;
	}
	/*
	 * For each finger present, read the proper number of registers
	 * to get absolute data.
	 */
	data_reg_blk_size = f11->size_of_data_register_block;
	for (finger = 0; finger < fingers_supported; finger++) {
		/* determine which data byte the finger status is in */
		reg = finger/4;
		/* bit shift to get finger's status */
		finger_shift	= (finger % 4) * 2;
		finger_status	= (values[reg] >> finger_shift) & 3;

		/*
		 * if finger status indicates a finger is present then
		 * read the finger data and report it
		 */

#ifdef SYNAPTICS_RMI4_USE_MULTITOUCH
		input_mt_slot(input, finger);
		input_mt_report_slot_state(input, MT_TOOL_FINGER,
					   inger_status != 0);
#else
		if (!finger_status)
			f11->contact_id[finger] = -1;
		else if (f11->contact_id[finger] == -1)
			f11->contact_id[finger] =
				f11->current_contact_id++ & CONTACT_ID_MAX;
#endif

		if (finger_status) {
			/* Read the finger data */
			data_offset = f11->fn.desc.data_base_addr +
					((finger * data_reg_blk_size) +
					finger_registers);
			error = synaptics_rmi4_i2c_block_read(rmi4,
						data_offset, data,
						data_reg_blk_size);
			if (error) {
				dev_err(rmi4->dev,
					"%s: read data failed: %d\n",
					__func__, error);
				return 0;
			}
			x = (data[0] << 4) | (data[2] & MASK_4BIT);
			y = (data[1] << 4) | ((data[2] >> 4) & MASK_4BIT);
			wy = (data[3] >> 4) & MASK_4BIT;
			wx = (data[3] & MASK_4BIT);
			z = data[4];

			if (rmi4->board->swap_xy) {
				swap(x, y);
				swap(wx, wy);
			}

			if (rmi4->board->x_flip)
				x = f11->sensor_max_x - x;
			if (rmi4->board->y_flip)
				y = f11->sensor_max_y - y;

#ifdef SYNAPTICS_RMI4_USE_MULTITOUCH
			input_report_abs(input,
					 ABS_MT_TOUCH_MAJOR, max(wx, wy));
			input_report_abs(input, ABS_MT_POSITION_X, x);
			input_report_abs(input, ABS_MT_POSITION_Y, y);
			input_report_abs(input, ABS_MT_PRESSURE, z);
#else
			/* Try to see if this finger is primary */
			if (st_id == -1 ||
			    ((f11->contact_id[finger] - st_id) & CONTACT_ID_SIGN)) {
				st_y = y;
				st_x = x;
				st_z = z;
				st_id = f11->contact_id[finger];
			}
#endif
			/* number of active touch points */
			touch_count++;
		}
	}

	/* sync after groups of events */
#ifdef SYNAPTICS_RMI4_USE_MULTITOUCH
	input_mt_sync_frame(input);
#else
	input_event(input, EV_KEY, BTN_TOUCH, touch_count > 0);
	input_event(input, EV_KEY, BTN_TOOL_FINGER, touch_count == 1);
	input_event(input, EV_KEY, BTN_TOOL_DOUBLETAP, touch_count == 2);
	input_event(input, EV_KEY, BTN_TOOL_TRIPLETAP, touch_count == 3);
	input_event(input, EV_KEY, BTN_TOOL_QUADTAP, touch_count == 4);
	if (touch_count > 0) {
		input_report_abs(input, ABS_X, st_x);
		input_report_abs(input, ABS_Y, st_y);
		input_report_abs(input, ABS_PRESSURE, st_z);
	} else {
		input_report_abs(input, ABS_PRESSURE, 0);
	}
#endif
	input_sync(input);
	/* return the number of touch points */
	return touch_count;
}

static ssize_t rmi_f11_query_data_read(struct file *data_file,
				       struct kobject *kobj,
				       struct bin_attribute *attr,
				       char *buf, loff_t pos, size_t count)
{
	struct device *dev = container_of(kobj, struct device, kobj);
	struct i2c_client *client = to_i2c_client(dev);
	struct synaptics_rmi4_data *rmi4 = i2c_get_clientdata(client);
	struct synaptics_rmi4_f11 *f11 = &rmi4->f11;

	if (count == 0)
		return 0;

	if (pos > F11_QUERY_LEN)
		return -EFBIG;

	if (pos + count > F11_QUERY_LEN)
		count = F11_QUERY_LEN - pos;

	memcpy(buf, f11->query_data + pos, count);
	return count;
}

static struct bin_attribute f11_query_data = {
	.attr = {
		 .name = "f11_query_data",
		 .mode = S_IRUGO
	},
	.size = 0,
	.read = rmi_f11_query_data_read,
};

static int synaptics_rmi4_setup_f11(struct synaptics_rmi4_data *rmi4,
				    struct synaptics_rmi4_fn *fn)
{
	struct synaptics_rmi4_f11 *f11 =
			container_of(fn, struct synaptics_rmi4_f11, fn);
	const struct synaptics_rmi4_platform_data *board = rmi4->board;
	struct input_dev *input;
	unsigned int x_size = board->x_size;
	unsigned int y_size = board->y_size;
	unsigned char data[BUF_LEN];
	int error;

#if 0	/* this doesn't make much sense */

	/* Get and print some info about the data source... */
	/* To Query 2D devices we need to read from the address obtained
	 * from the function descriptor stored in the RMI function info.
	 */
	error = synaptics_rmi4_i2c_block_read(rmi4, fn->desc.query_base_addr,
					      data, QUERY_LEN);
	if (error) {
		dev_err(rmi4->dev,
			"%s: read of query registers failed: %d\n",
			__func__, error);
		return error;
	}
#endif

	error = synaptics_rmi4_i2c_block_read(rmi4, fn->desc.ctrl_base_addr,
					      data, DATA_BUF_LEN);
	if (error) {
		dev_err(rmi4->dev,
			"%s: read of control registers failed: %d\n",
			__func__, error);
		return error;
	}

	if (x_size || y_size) {

		if (board->swap_xy)
			swap(x_size, y_size);

		if (x_size) {
			data[6] = x_size & MASK_8BIT;
			data[7] = (x_size >> 8) & MASK_4BIT;
		}

		if (y_size) {
			data[8] = y_size & MASK_8BIT;
			data[9] = (y_size >> 8) & MASK_4BIT;
		}

		error = synaptics_rmi4_i2c_block_write(rmi4,
					fn->desc.ctrl_base_addr + 6,
					data + 6, 4);
		if (error) {
			dev_err(rmi4->dev,
				"%s: write of control registers failed: %d\n",
				__func__, error);
			return error;
		}

		error = synaptics_rmi4_i2c_block_read(rmi4,
					fn->desc.ctrl_base_addr,
					data, DATA_BUF_LEN);
		if (error) {
			dev_err(rmi4->dev,
				"%s: 2nd read of control registers failed: %d\n",
				__func__, error);
			return error;
		}
	}

	/* Store these for use later */

	f11->sensor_max_x = ((data[6] & MASK_8BIT) << 0) |
					((data[7] & MASK_4BIT) << 8);
	f11->sensor_max_y = ((data[8] & MASK_5BIT) << 0) |
					((data[9] & MASK_4BIT) << 8);
	if (board->swap_xy)
		swap(f11->sensor_max_x, f11->sensor_max_y);

	input = input_allocate_device();
	if (!input)
		return -ENOMEM;

	input->name = DRIVER_NAME;
	input->phys = "Synaptics_Clearpad";
	input->id.bustype = BUS_I2C;
	input->dev.parent = rmi4->dev;
	input_set_drvdata(input, rmi4);

	/* Initialize the function handlers for rmi4 */
	__set_bit(EV_KEY, input->evbit);
	__set_bit(BTN_TOUCH, input->keybit);

	__set_bit(EV_ABS, input->evbit);

#ifdef SYNAPTICS_RMI4_USE_MULTITOUCH
	input_set_abs_params(input, ABS_MT_POSITION_X,
				0, f11->sensor_max_x, 0, 0);
	input_set_abs_params(input, ABS_MT_POSITION_Y,
				0, f11->sensor_max_y, 0, 0);
	input_set_abs_params(input, ABS_MT_TOUCH_MAJOR,
				0, MAX_TOUCH_MAJOR, 0, 0);
	input_set_abs_params(input, ABS_PRESSURE,
				0, MAX_PRESSURE, 0, 0);

	input_mt_init_slots(input, f11->num_of_data_points, 0);
#else
	input_set_abs_params(input, ABS_X,
				0, f11->sensor_max_x, 0, 0);
	input_set_abs_params(input, ABS_Y,
				0, f11->sensor_max_y, 0, 0);
	input_set_abs_params(input, ABS_PRESSURE,
				0, MAX_PRESSURE, 0, 0);

	__set_bit(BTN_TOOL_FINGER, input->keybit);
	__set_bit(BTN_TOOL_DOUBLETAP, input->keybit);
	if (f11->num_of_data_points >= 3)
		__set_bit(BTN_TOOL_TRIPLETAP, input->keybit);
	if (f11->num_of_data_points >= 4)
		__set_bit(BTN_TOOL_QUADTAP, input->keybit);
#endif

	error = sysfs_create_bin_file(&rmi4->dev->kobj, &f11_query_data);
	if (error < 0)
		return error;

	error = sysfs_create_bin_file(&rmi4->dev->kobj, &f11_control_data);
	if (error < 0)
		goto remove_query;

	error = input_register_device(input);
	if (error) {
		dev_err(rmi4->dev, "%s: Failed to register input device: %d\n",
			__func__, error);
		goto remove_control;
	}

	f11->input = input;

	return 0;

remove_control:
	sysfs_remove_bin_file(&rmi4->dev->kobj, &f11_control_data);
remove_query:
	sysfs_remove_bin_file(&rmi4->dev->kobj, &f11_query_data);
	return error;
}

static void synaptics_rmi4_destroy_f11(struct synaptics_rmi4_data *rmi4,
				       struct synaptics_rmi4_fn *fn)
{
	struct synaptics_rmi4_f11 *f11 =
			container_of(fn, struct synaptics_rmi4_f11, fn);

	/* The interrupt source should be disabled by this time */
	input_unregister_device(f11->input);

	sysfs_remove_bin_file(&rmi4->dev->kobj, &f11_query_data);
	sysfs_remove_bin_file(&rmi4->dev->kobj, &f11_control_data);
}

static void synaptics_rmi4_destroy_fns(struct synaptics_rmi4_data *rmi4)
{
	struct synaptics_rmi4_fn *fn, *tmp;

	list_for_each_entry_safe(fn, tmp, &rmi4->fn_list, link) {
		synaptics_rmi4_toggle_ints_for_fn(rmi4, fn, false);
		list_del_init(&fn->link);
		if (fn->destroy)
			fn->destroy(rmi4, fn);
	}
}

/**
 * synaptics_rmi4_parse_f11() - detects the rmi4 touchpad device
 * @pdata: pointer to synaptics_rmi4_data structure
 * @fd: pointer to synaptics_rmi4_fn_desc structure
 *
 * This function calls to detects the rmi4 touchpad device
 */
static int synaptics_rmi4_parse_f11(struct synaptics_rmi4_data *rmi4,
				    struct synaptics_rmi4_fn_desc *fd)
{
	struct synaptics_rmi4_f11 *f11;
	unsigned char	*queries;
	unsigned char	abs_data_size;
	unsigned char	abs_data_blk_size;
	unsigned char	egr_0, egr_1;
	unsigned int	all_data_blk_size;
	int	has_pinch, has_flick, has_tap;
	int	has_tapandhold, has_doubletap;
	int	has_earlytap, has_press;
	int	has_palmdetect, has_rotate;
	int	has_rel;
	int	error;
	bool	has_query12;
	bool	has_adjustable_mapping = false;
	int	num_rx_electrodes, num_tx_electrodes;

	f11 = &rmi4->f11;
	queries = f11->query_data;

	/*
	 * need to get number of fingers supported, data size, etc.
	 * to be used when getting data since the number of registers to
	 * read depends on the number of fingers supported and data size.
	 */
	error = synaptics_rmi4_i2c_block_read(rmi4, fd->query_base_addr,
					      f11->query_data,
					      sizeof(f11->query_data));
	if (error) {
		dev_err(rmi4->dev,
			"%s: read function query registers: %d\n",
			__func__, error);
		return error;
	}

	f11->fn.desc = *fd;
	f11->fn.intr_offset = rmi4->interrupt_count;
	f11->fn.setup = synaptics_rmi4_setup_f11;
	f11->fn.destroy = synaptics_rmi4_destroy_f11;
	f11->fn.interrupt_handler = synaptics_rmi4_touchpad_report;

	/*
	 * 2D data sources have only 3 bits for the number of fingers
	 * supported - so the encoding is a bit weird.
	 */
	if ((queries[1] & MASK_3BIT) <= 4) {
		/* add 1 since zero based */
		f11->num_of_data_points = (queries[1] & MASK_3BIT) + 1;
	} else {
		/*
		 * a value of 5 is up to 10 fingers - 6 and 7 are reserved
		 * (shouldn't get these i int error;n a normal 2D source).
		 */
		if ((queries[1] & MASK_3BIT) == 5)
			f11->num_of_data_points = 10;
	}

	/* Size of just the absolute data for one finger */
	abs_data_size = queries[5] & MASK_2BIT;
	/* One each for X and Y, one for LSB for X & Y, one for W, one for Z */
	abs_data_blk_size = 3 + (2 * (abs_data_size == 0 ? 1 : 0));
	f11->size_of_data_register_block = abs_data_blk_size;

	/*
	 * need to determine the size of data to read - this depends on
	 * conditions such as whether Relative data is reported and if Gesture
	 * data is reported.
	 */
	egr_0 = queries[7];
	egr_1 = queries[8];

	/*
	 * Get info about what EGR data is supported, whether it has
	 * Relative data supported, etc.
	 */
	has_pinch	= egr_0 & HAS_PINCH;
	has_flick	= egr_0 & HAS_FLICK;
	has_tap		= egr_0 & HAS_TAP;
	has_earlytap	= egr_0 & HAS_EARLYTAP;
	has_press	= egr_0 & HAS_PRESS;
	has_rotate	= egr_1 & HAS_ROTATE;
	has_rel		= queries[1] & HAS_RELEASE;
	has_tapandhold	= egr_0 & HAS_TAPANDHOLD;
	has_doubletap	= egr_0 & HAS_DOUBLETAP;
	has_palmdetect	= egr_1 & HAS_PALMDETECT;

	has_query12 = queries[0] & HAS_QUERY12;
	if (has_query12)
		has_adjustable_mapping = queries[12] & HAS_ADJUSTABLE_MAPPING;
	num_rx_electrodes = queries[3];
	num_tx_electrodes = queries[2];

	if (has_adjustable_mapping) {
		u8 ctrl[2];

		dev_dbg(rmi4->dev,
			"F11: Electrode mapping is adjustable, trying to read it\n");
		error = synaptics_rmi4_i2c_block_read(rmi4,
						      fd->ctrl_base_addr + 77,
						      ctrl, 2);
		if (!error) {
			dev_dbg(rmi4->dev, "F11: rx electrodes: %d -> %d\n",
				 num_rx_electrodes, ctrl[0]);
			dev_dbg(rmi4->dev, "F11: tx electrodes: %d -> %d\n",
				 num_tx_electrodes, ctrl[1]);
			num_rx_electrodes = ctrl[0];
			num_tx_electrodes = ctrl[1];
		} else {
			dev_err(rmi4->dev,
				"F11: Failed to read adjusted # of electrodes: %d\n",
				error);
		}
	}
	dev_dbg(rmi4->dev, "F11: rx electrodes: %d, tx electrodes: %d\n",
		num_rx_electrodes, num_tx_electrodes);

	/*
	 * Size of all data including finger status, absolute data for each
	 * finger, relative data and EGR data
	 */
	all_data_blk_size =
		/* finger status, four fingers per register */
		((f11->num_of_data_points + 3) / 4) +
		/* absolute data, per finger times number of fingers */
		(abs_data_blk_size * f11->num_of_data_points) +
		/*
		 * two relative registers (if relative is being reported)
		 */
		2 * has_rel +
		/*
		 * F11_2D_data8 is only present if the egr_0
		 * register is non-zero.
		 */
		!!(egr_0) +
		/*
		 * F11_2D_data9 is only present if either egr_0 or
		 * egr_1 registers are non-zero.
		 */
		(egr_0 || egr_1) +
		/*
		 * F11_2D_data10 is only present if EGR_PINCH or EGR_FLICK of
		 * egr_0 reports as 1.
		 */
		!!(has_pinch | has_flick) +
		/*
		 * F11_2D_data11 and F11_2D_data12 are only present if
		 * EGR_FLICK of egr_0 reports as 1.
		 */
		2 * !!(has_flick);

#ifndef SYNAPTICS_RMI4_USE_MULTITOUCH
	{
		int i;
		for (i = 0; i < SYNAPTICS_RMI4_MAX_FINGERS; i++)
			f11->contact_id[i] = -1;
	}
#endif

	list_add_tail(&f11->fn.link, &rmi4->fn_list);
	return 0;
}

/**
 * synaptics_rmi4_query_device() - query the rmi4 device
 * @pdata: pointer to synaptics_rmi4_data structure
 *
 * This function is used to query the rmi4 device.
 */
static int synaptics_rmi4_query_device(struct synaptics_rmi4_data *rmi4)
{
	struct synaptics_rmi4_fn *fn;
	int error;

	error = synaptics_rmi4_scan_pdt(rmi4, true);
	if (error) {
		/*
		 * If there was an error, try resetting the device and try
		 * again. The error may be due to the device being in wrong
		 * state (specifically in fw download state).
		 */
		synaptics_rmi4_reset_device(rmi4);
		synaptics_rmi4_destroy_fns(rmi4);

		memset(&rmi4->f01, 0, sizeof(rmi4->f01));
		memset(&rmi4->f11, 0, sizeof(rmi4->f11));
		memset(&rmi4->f34, 0, sizeof(rmi4->f34));
		memset(&rmi4->f54, 0, sizeof(rmi4->f54));
		memset(&rmi4->f55, 0, sizeof(rmi4->f55));

		error = synaptics_rmi4_scan_pdt(rmi4, true);
		if (error)
			goto err_out;
	}

	list_for_each_entry(fn, &rmi4->fn_list, link) {
		if (fn->setup) {
			error = fn->setup(rmi4, fn);
			if (error)
				goto err_out;
		}

		error = synaptics_rmi4_toggle_ints_for_fn(rmi4, fn,
						fn->interrupt_handler != NULL);
		if (error) {
			dev_err(rmi4->dev,
				"%s: Failed to enable interrupts for F%02x: %d\n",
				__func__, fn->desc.fn_number, error);
			goto err_out;
		}
	}

	return 0;

err_out:
	synaptics_rmi4_destroy_fns(rmi4);
	return error;
}

static struct synaptics_rmi4_platform_data synaptics_rmi4_platformdata = {
	.irq_type       = (IRQF_TRIGGER_FALLING | IRQF_SHARED),
	.x_size		= 1280,
	.y_size		= 800,
	.x_flip		= false,
	.y_flip		= true,
	.swap_xy	= true,
};

/**
 * synaptics_rmi4_probe() - Initialze the i2c-client touchscreen driver
 * @i2c: i2c client structure pointer
 * @id:i2c device id pointer
 *
 * This function will allocate and initialize the instance
 * data and request the irq and set the instance data as the clients
 * platform data then register the physical driver which will do a scan of
 * the rmi4 Physical Device Table and enumerate any rmi4 functions that
 * have data sources associated with them.
 */
static int synaptics_rmi4_probe(struct i2c_client *client,
				const struct i2c_device_id *dev_id)
{
	const struct synaptics_rmi4_platform_data *platformdata =
						client->dev.platform_data;
	struct synaptics_rmi4_data *rmi4;
	unsigned char intr_status[4];
	int error;

	if (!i2c_check_functionality(client->adapter,
				     I2C_FUNC_SMBUS_BYTE_DATA | I2C_FUNC_I2C)) {
		dev_err(&client->dev, "i2c smbus byte data not supported\n");
		return -EIO;
	}

	if (!platformdata) {
		platformdata = &synaptics_rmi4_platformdata;
		//dev_err(&client->dev, "%s: no platform data\n", __func__);
		//return -EINVAL;
	}

	/* Allocate and initialize the instance data for this client */
	rmi4 = devm_kzalloc(&client->dev,
			    sizeof(struct synaptics_rmi4_data), GFP_KERNEL);
	if (!rmi4) {
		dev_err(&client->dev, "%s: no memory allocated\n", __func__);
		return -ENOMEM;
	}

	if (platformdata->regulator_name) {
		rmi4->regulator = regulator_get(&client->dev, "vdd");
		if (IS_ERR(rmi4->regulator)) {
			error = PTR_ERR(rmi4->regulator);
			dev_err(&client->dev,
				"%s: get regulator failed: %d\n",
				__func__, error);
			return error;
		}

		error = regulator_enable(rmi4->regulator);
		if (error < 0) {
			dev_err(&client->dev,
				"%s: regulator enable failed: %d\n",
				__func__, error);
			goto err_put_regulator;
		}
	}

	rmi4->dev = &client->dev;
	rmi4->i2c_client = client;

	/* So we set the page correctly the first time */
	rmi4->current_page = MASK_16BIT;

	rmi4->board = platformdata;
	rmi4->touch_stopped = false;

	init_waitqueue_head(&rmi4->wait);

	mutex_init(&rmi4->rmi4_page_mutex);
	mutex_init(&rmi4->flash_mutex);
	init_completion(&rmi4->async_firmware_done);

	INIT_LIST_HEAD(&rmi4->fn_list);

	/*
	 * Register physical driver - this will call the detect function that
	 * will then scan the device and determine the supported
	 * rmi4 functions.
	 */
	error = synaptics_rmi4_query_device(rmi4);
	if (error) {
		dev_err(&client->dev,
			"%s: rmi4 query device failed: %d\n",
			__func__, error);
		goto err_disable_regulator;
	}

	/* Store the instance data in the i2c_client */
	i2c_set_clientdata(client, rmi4);

	/* Clear interrupts */
	synaptics_rmi4_i2c_block_read(rmi4,
			rmi4->f01.fn.desc.data_base_addr + 1, intr_status,
			rmi4->n_interrupt_registers);

	if (client->irq) {
		error = request_threaded_irq(client->irq, NULL,
					synaptics_rmi4_irq,
					platformdata->irq_type | IRQF_ONESHOT,
					DRIVER_NAME, rmi4);
		if (error) {
			dev_err(&client->dev,
				"%s: Unable to get attn irq %d: %d\n",
				__func__, client->irq, error);
			goto err_destroy_fns;
		}
	}

	error = sysfs_create_group(&client->dev.kobj,
				   &synaptics_rmi4_attr_group);
	if (error) {
		dev_err(&client->dev,
			"%s: failed to create sysfs attributes: %d\n",
			__func__, error);
		goto err_free_irq;
	}

	/* See if the device in bootloader mode */
	if (rmi4->f01.status & 0x40) {
		dev_info(&client->dev,
			 "%s: device is in bootloader mode (%#02x), please run firmware update\n",
			__func__, rmi4->f01.status);
		/*
		 * This error is not fatal, let userspace have
		 * another chance.
		 */
		complete(&rmi4->async_firmware_done);
	}

	return 0;

err_free_irq:
	if (client->irq)
		free_irq(client->irq, rmi4);
err_destroy_fns:
	synaptics_rmi4_destroy_fns(rmi4);
err_disable_regulator:
	if (rmi4->regulator)
		regulator_disable(rmi4->regulator);
err_put_regulator:
	if (rmi4->regulator)
		regulator_put(rmi4->regulator);
	return error;
}
/**
 * synaptics_rmi4_remove() - Removes the i2c-client touchscreen driver
 * @client: i2c client structure pointer
 *
 * This function uses to remove the i2c-client
 * touchscreen driver and returns integer.
 */
static int synaptics_rmi4_remove(struct i2c_client *client)
{
	struct synaptics_rmi4_data *rmi4 = i2c_get_clientdata(client);

	/* Make sure our initial firmware request has completed */
	wait_for_completion(&rmi4->async_firmware_done);

	sysfs_remove_group(&client->dev.kobj, &synaptics_rmi4_attr_group);

	rmi4->touch_stopped = true;
	mb();
	wake_up(&rmi4->wait);

	if (client->irq)
		free_irq(client->irq, rmi4);

	synaptics_rmi4_destroy_fns(rmi4);

	if (rmi4->regulator) {
		regulator_disable(rmi4->regulator);
		regulator_put(rmi4->regulator);
	}

	return 0;
}

#ifdef CONFIG_PM
/**
 * synaptics_rmi4_suspend() - suspend the touch screen controller
 * @dev: pointer to device structure
 *
 * This function is used to suspend the
 * touch panel controller and returns integer
 */
static int synaptics_rmi4_suspend(struct device *dev)
{
	struct i2c_client *client = to_i2c_client(dev);
	struct synaptics_rmi4_data *rmi4 = i2c_get_clientdata(client);
	unsigned char intr_status;
	int error;

	rmi4->touch_stopped = true;
	mb();
	disable_irq(client->irq);

	error = synaptics_rmi4_i2c_block_read(rmi4,
				rmi4->f01.fn.desc.data_base_addr + 1,
				&intr_status,
				rmi4->n_interrupt_registers);
	if (error)
		return error;

	error = synaptics_rmi4_i2c_byte_write(rmi4,
					rmi4->f01.fn.desc.ctrl_base_addr + 1,
					(intr_status & ~TOUCHPAD_CTRL_INTR));
	if (error < 0)
		return error;

	if (rmi4->regulator)
		regulator_disable(rmi4->regulator);

	return 0;
}
/**
 * synaptics_rmi4_resume() - resume the touch screen controller
 * @dev: pointer to device structure
 *
 * This function is used to resume the touch panel
 * controller and returns integer.
 */
static int synaptics_rmi4_resume(struct device *dev)
{
	struct i2c_client *client = to_i2c_client(dev);
	struct synaptics_rmi4_data *rmi4 = i2c_get_clientdata(client);
	unsigned char intr_status;
	int error;

	if (rmi4->regulator)
		regulator_enable(rmi4->regulator);

	rmi4->touch_stopped = false;
	enable_irq(client->irq);

	error = synaptics_rmi4_i2c_block_read(rmi4,
				rmi4->f01.fn.desc.data_base_addr + 1,
				&intr_status,
				rmi4->n_interrupt_registers);
	if (error)
		return error;

	error = synaptics_rmi4_i2c_byte_write(rmi4,
					rmi4->f01.fn.desc.ctrl_base_addr + 1,
					(intr_status | TOUCHPAD_CTRL_INTR));
	if (error)
		return error;

	return 0;
}

static const struct dev_pm_ops synaptics_rmi4_dev_pm_ops = {
	.suspend = synaptics_rmi4_suspend,
	.resume  = synaptics_rmi4_resume,
};
#endif

static const struct i2c_device_id synaptics_rmi4_id_table[] = {
	{ DRIVER_NAME, 0 },
	{ },
};
MODULE_DEVICE_TABLE(i2c, synaptics_rmi4_id_table);

static struct i2c_driver synaptics_rmi4_driver = {
	.driver = {
		.name	=	DRIVER_NAME,
		.owner	=	THIS_MODULE,
#ifdef CONFIG_PM
		.pm	=	&synaptics_rmi4_dev_pm_ops,
#endif
	},
	.probe		=	synaptics_rmi4_probe,
	.remove		=	synaptics_rmi4_remove,
	.id_table	=	synaptics_rmi4_id_table,
};

static int __init synaptics_rmi4_init(void)
{
	return i2c_add_driver(&synaptics_rmi4_driver);
}
module_init(synaptics_rmi4_init);

static void __exit synaptics_rmi4_exit(void)
{
	i2c_del_driver(&synaptics_rmi4_driver);
}
module_exit(synaptics_rmi4_exit);

MODULE_LICENSE("GPL v2");
MODULE_AUTHOR("naveen.gaddipati@stericsson.com, js.ha@stericsson.com");
MODULE_DESCRIPTION("synaptics rmi4 i2c touch Driver");
MODULE_ALIAS("i2c:synaptics_rmi4_ts");
